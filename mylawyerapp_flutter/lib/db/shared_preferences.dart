//import 'dart:math';
import 'dart:collection';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypted_shared_preferences/encrypted_shared_preferences.dart';
//import 'package:mylawyerapp_flutter/main.dart'

class UserPreferences {
  static final UserPreferences _singleton = UserPreferences._internal();
  factory UserPreferences() {
    return _singleton;
  }

  UserPreferences._internal();

  // Firebase
  Future<bool> deletePreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    bool ft = await preferences.remove('FirebaseToken');
    bool gc = await preferences.remove('GlobalClient');
    bool ui = await preferences.remove('UserId');
    bool ed = await preferences.remove('ExpiryDate');
    bool ud = await preferences.remove('UserData');
    if (ft && gc && ui && ed && ud) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> containsKey(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }

  Future<bool?> getDarkMode() async {
    return (await SharedPreferences.getInstance())
        .getBool(UserPreferenceKey.DarkMode.toString());
  }

  // Firebase
  Future<bool> setDarkMode(bool value) async {
    return (await SharedPreferences.getInstance())
        .setBool(UserPreferenceKey.DarkMode.toString(), value);
  }

  Future<String?> getUserData() async {
    return (await SharedPreferences.getInstance())
        .getString(UserPreferenceKey.UserData.toString());
  }

  // Firebase
  Future<bool> setUserData(String value) async {
    return (await SharedPreferences.getInstance())
        .setString(UserPreferenceKey.UserData.toString(), value);
  }

  Future<String?> getFirebaseToken() async {
    return (await SharedPreferences.getInstance())
        .getString(UserPreferenceKey.FirebaseToken.toString());
  }

  // Firebase
  Future<bool> setFirebaseToken(String value) async {
    return (await SharedPreferences.getInstance())
        .setString(UserPreferenceKey.FirebaseToken.toString(), value);
  }

  Future<String?> getUserId() async {
    return (await SharedPreferences.getInstance())
        .getString(UserPreferenceKey.UserId.toString());
  }

  // Firebase
  Future<bool> setUserId(String value) async {
    return (await SharedPreferences.getInstance())
        .setString(UserPreferenceKey.UserId.toString(), value);
  }

  Future<String?> getExpiryDate() async {
    return (await SharedPreferences.getInstance())
        .getString(UserPreferenceKey.ExpiryDate.toString());
  }

  // Firebase
  Future<bool> setExpiryDate(String value) async {
    return (await SharedPreferences.getInstance())
        .setString(UserPreferenceKey.ExpiryDate.toString(), value);
  }

  Future<String?> getGlobalClient() async {
    final prefs = await SharedPreferences.getInstance();
    EncryptedSharedPreferences encryptedSharedPreferences =
        EncryptedSharedPreferences(prefs: prefs);

    return encryptedSharedPreferences
        .getString(UserPreferenceKey.GlobalClient.toString());
  }

  // Local
  Future<bool> setGlobalClient(String value) async {
    final prefs = await SharedPreferences.getInstance();
    EncryptedSharedPreferences encryptedSharedPreferences =
        EncryptedSharedPreferences(prefs: prefs);

    return encryptedSharedPreferences.setString(
        UserPreferenceKey.GlobalClient.toString(), value);
  }

  Future<bool?> getLastUpdatedContent() async {
    final prefs = await SharedPreferences.getInstance();
    final lastUpdatedContent =
        prefs.getBool(UserPreferenceKey.LastUpdatedContent.toString());
    // returns null if content has never been updated before
    return lastUpdatedContent;
  }

  Future<bool> setLastUpdatedContent(bool loaded) async {
    final prefs = await SharedPreferences.getInstance();
    final update = await prefs.setBool(
        UserPreferenceKey.LastUpdatedContent.toString(), loaded);

    return update;
  }

  Future<String?> getProfileImg() async {
    final prefs = await SharedPreferences.getInstance();
    EncryptedSharedPreferences encryptedSharedPreferences =
        EncryptedSharedPreferences(prefs: prefs);

    return encryptedSharedPreferences
        .getString(UserPreferenceKey.UserImageProfile.toString());
  }

  // Local
  Future<bool> setProfileImg(String value) async {
    final prefs = await SharedPreferences.getInstance();
    EncryptedSharedPreferences encryptedSharedPreferences =
        EncryptedSharedPreferences(prefs: prefs);

    return encryptedSharedPreferences.setString(
        UserPreferenceKey.UserImageProfile.toString(), value);
  }
}

enum UserPreferenceKey {
  LastUpdatedContent,
  UserData,
  FirebaseToken,
  UserImageProfile,
  GlobalClient,
  UserId,
  ExpiryDate,
  DarkMode
}
