import 'package:mylawyerapp_flutter/models/appointmentDTO.dart';
import 'package:mylawyerapp_flutter/models/caseDTO.dart';
import 'package:mylawyerapp_flutter/models/lawyerDTO.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class LawyersDatabase {
  static final LawyersDatabase instance = LawyersDatabase._init();

  static Database? _database;

  LawyersDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB('lawyers.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'STRING PRIMARY KEY';
    final textType = 'TEXT NOT NULL';
    final doubleType = 'DOUBLE NOT NULL';
    final integerType = 'INTEGER NOT NULL';

    await db.execute('''
    CREATE TABLE $tableAppointments(
      ${AppointmentFields.id} $idType,
      ${AppointmentFields.dateHour} $textType,
      ${AppointmentFields.description} $textType,
      ${AppointmentFields.clientId} $textType,
      ${AppointmentFields.lawyerId} $textType
    )
    ''');

    await db.execute('''
    CREATE TABLE $tableCases(
      ${CaseFields.id} $idType,
      ${CaseFields.startDate} $textType,
      ${CaseFields.description} $textType,
      ${CaseFields.type} $textType,
      ${CaseFields.status} $textType,
      ${CaseFields.clientId} $textType,
      ${CaseFields.lawyerId} $textType,
      ${CaseFields.endDate} $textType,
      ${CaseFields.lawyerName} $textType
    )
    ''');

    await db.execute('''
    CREATE TABLE $tableLawyers(
      ${LawyersFields.id} $idType,
      ${LawyersFields.idAuth} $textType,
      ${LawyersFields.name} $textType,
      ${LawyersFields.phoneNumber} $textType,
      ${LawyersFields.emailAddress} $textType,
      ${LawyersFields.casesWon} $integerType,
      ${LawyersFields.field} $textType,
      ${LawyersFields.hourlyFee} $doubleType,
      ${LawyersFields.rating} $integerType,
      ${LawyersFields.numberOfRatings} $integerType,
      ${LawyersFields.latitude} $textType,
      ${LawyersFields.longitude} $textType,
      ${LawyersFields.views} $integerType,
      ${LawyersFields.yearOfExperience} $textType,
      ${LawyersFields.distance} $integerType
    )
    ''');
  }

  Future<void> createLawyer(Lawyer lawyer) async {
    final db = await instance.database;
    //final id = await db.insert(tableLawyers, lawyer.toJson());
    await db.insert(tableLawyers, lawyer.toJson());
    //return lawyer.copy(id: id);
  }

  Future<void> createAppointment(Appointment appointment) async {
    final db = await instance.database;
    //final id = await db.insert(tableLawyers, lawyer.toJson());
    await db.insert(tableAppointments, appointment.toJson());
    //return lawyer.copy(id: id);
  }

  Future<void> createCase(Case pCase) async {
    final db = await instance.database;
    //final id = await db.insert(tableLawyers, lawyer.toJson());
    await db.insert(tableCases, pCase.toJson());
    //return lawyer.copy(id: id);
  }

  Future<Lawyer> readLawyer(String idAuth) async {
    final db = await instance.database;
    final maps = await db.query(
      tableLawyers,
      columns: LawyersFields.values,
      where: '${LawyersFields.idAuth} = ?',
      whereArgs: [idAuth],
    );

    if (maps.isNotEmpty) {
      return Lawyer.fromJson2(maps.first);
    } else {
      throw Exception('idAuth $idAuth not found');
    }
  }

  Future<Appointment> readAppointment(String id) async {
    final db = await instance.database;
    final maps = await db.query(
      tableAppointments,
      columns: AppointmentFields.values,
      where: '${AppointmentFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return Appointment.fromJson2(maps.first);
    } else {
      throw Exception('id $id not found');
    }
  }

  Future<Case> readCase(String id) async {
    final db = await instance.database;
    final maps = await db.query(
      tableCases,
      columns: CaseFields.values,
      where: '${CaseFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return Case.fromJson2(maps.first);
    } else {
      throw Exception('id $id not found');
    }
  }

  Future<List<Lawyer>> readAllLawyers() async {
    final db = await instance.database;
    final result = await db.query(tableLawyers);

    return result.map((json) => Lawyer.fromJson2(json)).toList();
  }

  Future<List<Appointment>> readAllAppointments() async {
    final db = await instance.database;
    final result = await db.query(tableAppointments);

    return result.map((json) => Appointment.fromJson2(json)).toList();
  }

  Future<List<Case>> readAllCases() async {
    final db = await instance.database;
    final result = await db.query(tableCases);

    return result.map((json) => Case.fromJson2(json)).toList();
  }

  Future<void> updateLawyer(Lawyer lawyer) async {
    final db = await instance.database;

    db.update(
      tableLawyers,
      lawyer.toJson(),
      where: '${LawyersFields.idAuth} = ?',
      whereArgs: [lawyer.idAuth],
    );
  }

  Future<void> updateAppointment(Appointment appointment) async {
    final db = await instance.database;

    db.update(
      tableAppointments,
      appointment.toJson(),
      where: '${AppointmentFields.id} = ?',
      whereArgs: [appointment.id],
    );
  }

  Future<void> updateCases(Case pCase) async {
    final db = await instance.database;

    db.update(
      tableCases,
      pCase.toJson(),
      where: '${CaseFields.id} = ?',
      whereArgs: [pCase.id],
    );
  }

  Future<void> deleteLawyer(String idAuth) async {
    final db = await instance.database;

    await db.delete(
      tableLawyers,
      where: '${LawyersFields.idAuth} = ?',
      whereArgs: [idAuth],
    );
  }

  Future<void> deleteAppointment(String id) async {
    print("borrando appointment...");
    final db = await instance.database;

    await db.delete(
      tableAppointments,
      where: '${AppointmentFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future<void> deleteCase(String id) async {
    print("borrando case...");
    final db = await instance.database;

    await db.delete(
      tableCases,
      where: '${CaseFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
