import 'package:flutter/material.dart';
import 'package:theme_provider/theme_provider.dart';

import 'constants.dart';

class MyThemes {
  static final primaryColor = cBrown;
  static final darkTheme = AppTheme(
      id: "custom_dark", // Id(or name) of the theme(Has to be unique)
      description: "Dark Theme", // Description of theme
      data: ThemeData(
        scaffoldBackgroundColor: cGray,
        primaryColor : cBlack,
        primaryColorDark: primaryColor,
        dividerColor: Colors.white,
        colorScheme: ColorScheme.dark(),
        accentColor: cWhite,
        buttonColor: cBlack,
        iconTheme: IconThemeData(color:cWhite),
        buttonTheme: ButtonThemeData(buttonColor: cWhite),
        //textButtonTheme: TextButtonThemeData(style: TextButton.styleFrom(primary: cWhite)),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(backgroundColor: cBlack),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: 'AndadaPro',
      ));

  static final lightTheme = AppTheme(
      id: "custom_light", // Id(or name) of the theme(Has to be unique)
      description: "Light Theme", // Description of theme
      data: ThemeData(
          accentColor: cGray,
          primaryColor: primaryColor,
          dividerColor: Colors.black,
          buttonColor: cBrown,
          iconTheme: IconThemeData(color: cGray),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(backgroundColor: primaryColor),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'AndadaPro'));
}
