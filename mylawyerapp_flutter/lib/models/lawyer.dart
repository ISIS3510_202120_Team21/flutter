class Lawyer {
  late final String idAuth;
  late final String name;
  late final String phoneNumber;
  late final String emailAddress;
  late final int casesWon;
  late final String field;
  late final double hourlyFee;
  late final String rating;
  late final int numberOfRatings;
  late final double latitude;
  late final double longitude;
  late final int distance;

  Lawyer(
      {required this.idAuth,
      required this.name,
      required this.phoneNumber,
      required this.emailAddress,
      required this.casesWon,
      required this.field,
      required this.hourlyFee,
      required this.rating,
      required this.numberOfRatings,
      required this.latitude,
      required this.longitude,
      required this.distance,
      });
}
