final String tableLawyers = 'lawyers';

class LawyersFields {
  static final List<String> values = [
    //Add all fields
    id, idAuth, name, phoneNumber, emailAddress, yearOfExperience, casesWon,
    rating, hourlyFee, field, numberOfRatings, latitude, longitude, views, distance,
  ];

  static final String id = '_id';
  static final String idAuth = 'idAuth';
  static final String name = 'name';
  static final String phoneNumber = 'phoneNumber';
  static final String emailAddress = 'emailAddress';
  static final String yearOfExperience = 'yearOfExperience';
  static final String casesWon = 'casesWon';
  static final String rating = 'rating';
  static final String hourlyFee = 'hourlyFee';
  static final String field = 'field';
  static final String numberOfRatings = 'numberOfRatings';
  static final String latitude = 'latitude';
  static final String longitude = 'longitude';
  static final String views = 'views';
  static final String distance = 'distance';
}

class Lawyer {
  late String id;
  late String idAuth;
  late String name;
  late String phoneNumber;
  late String emailAddress;
  late String yearOfExperience;
  late int casesWon;
  late int rating;
  late double hourlyFee;
  late String field;
  late int numberOfRatings;
  late String latitude;
  late String longitude;
  late int views;
  late int distance;

  Lawyer({
    required this.id,
    required this.idAuth,
    required this.name,
    required this.phoneNumber,
    required this.emailAddress,
    required this.yearOfExperience,
    required this.casesWon,
    required this.rating,
    required this.hourlyFee,
    required this.field,
    required this.numberOfRatings,
    required this.latitude,
    required this.longitude,
    required this.views,
    required this.distance,
  });

  Lawyer.fromJson(Map<String, dynamic> json) {
    this.id = json['_id'];
    this.idAuth = json['idAuth'];
    this.name = json['name'];
    this.phoneNumber = json['phoneNumber'];
    this.emailAddress = json['emailAddress'];
    this.yearOfExperience = json['yearOfExperience'];
    this.casesWon = json['casesWon'];
    this.rating = int.parse(json['rating']);
    this.hourlyFee = double.parse(json['hourlyFee']);
    this.field = json['field'];
    this.numberOfRatings = json['numberOfRatings'];
    this.latitude = json['latitude'];
    this.longitude = json['longitude'];
    this.views = json['views'];
    this.distance = int.parse(json['distance']);
  }

  static Lawyer fromJson2(Map<String, Object?> json) => Lawyer(
        id: json[LawyersFields.id] as String,
        idAuth: json[LawyersFields.idAuth] as String,
        name: json[LawyersFields.name] as String,
        phoneNumber: json[LawyersFields.phoneNumber] as String,
        emailAddress: json[LawyersFields.emailAddress] as String,
        yearOfExperience: json[LawyersFields.yearOfExperience] as String,
        casesWon: json[LawyersFields.casesWon] as int,
        rating: json[LawyersFields.rating] as int,
        hourlyFee: json[LawyersFields.hourlyFee] as double,
        field: json[LawyersFields.field] as String,
        numberOfRatings: json[LawyersFields.numberOfRatings] as int,
        latitude: json[LawyersFields.latitude] as String,
        longitude: json[LawyersFields.longitude] as String,
        views: json[LawyersFields.views] as int,
        distance: json[LawyersFields.distance] as int,
      );

  Map<String, Object?> toJson() => {
        LawyersFields.id: id,
        LawyersFields.idAuth: idAuth,
        LawyersFields.name: name,
        LawyersFields.phoneNumber: phoneNumber,
        LawyersFields.emailAddress: emailAddress,
        LawyersFields.yearOfExperience: yearOfExperience,
        LawyersFields.casesWon: casesWon,
        LawyersFields.rating: rating,
        LawyersFields.hourlyFee: hourlyFee,
        LawyersFields.field: field,
        LawyersFields.numberOfRatings: numberOfRatings,
        LawyersFields.latitude: latitude,
        LawyersFields.longitude: longitude,
        LawyersFields.views: views,
        LawyersFields.distance: distance,
      };

  Lawyer copy({
    String? id,
    String? idAuth,
    String? name,
    String? phoneNumber,
    String? emailAddress,
    String? yearOfExperience,
    int? casesWon,
    int? rating,
    double? hourlyFee,
    String? field,
    int? numberOfRatings,
    String? latitude,
    String? longitude,
    int? views,
    int? distance,
  }) =>
      Lawyer(
        id: id ?? this.id,
        idAuth: idAuth ?? this.id,
        name: name ?? this.name,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        emailAddress: emailAddress ?? this.emailAddress,
        yearOfExperience: yearOfExperience ?? this.yearOfExperience,
        casesWon: casesWon ?? this.casesWon,
        rating: rating ?? this.rating,
        hourlyFee: hourlyFee ?? this.hourlyFee,
        field: field ?? this.field,
        numberOfRatings: numberOfRatings ?? this.numberOfRatings,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
        views: views ?? this.views,
        distance: distance ?? this.distance,
      );

  @override
  bool operator ==(other) {
    return (other is Lawyer) &&
        other.id == id &&
        other.idAuth == idAuth &&
        other.name == other.name &&
        other.phoneNumber == phoneNumber &&
        other.emailAddress == emailAddress &&
        other.yearOfExperience == yearOfExperience &&
        other.casesWon == casesWon &&
        other.rating == rating &&
        other.hourlyFee == hourlyFee &&
        other.field == field &&
        other.numberOfRatings == numberOfRatings &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.views == views &&
        other.distance == distance;
  }

  @override
  int get hashCode =>
      id.hashCode ^
      idAuth.hashCode ^
      name.hashCode ^
      phoneNumber.hashCode ^
      emailAddress.hashCode ^
      yearOfExperience.hashCode ^
      casesWon.hashCode ^
      rating.hashCode ^
      hourlyFee.hashCode ^
      field.hashCode ^
      numberOfRatings.hashCode ^
      latitude.hashCode ^
      longitude.hashCode ^
      views.hashCode ^
      distance.hashCode;
}
