class Client {
  late String id;
  late String name;
  late int phoneNumber;
  late String emailAddress;
  late bool hasMeetings;
  late int contactedLawyersEmail;
  late int contactedLawyersPhone;
  late int appointmentsDone;
  late int lawyersChecked;
  late String img;
  late int canceledLawyers;

  Client(
      {required this.id,
      required this.name,
      required this.phoneNumber,
      required this.emailAddress,
      required this.hasMeetings,
      required this.contactedLawyersEmail,
      required this.contactedLawyersPhone,
      required this.appointmentsDone,
      required this.lawyersChecked,
      required this.img,
      required this.canceledLawyers});

  Client.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.phoneNumber = int.parse(json['phoneNumber']);
    this.emailAddress = json['emailAddress'];
    this.hasMeetings = json['hasMeetings'];
    this.contactedLawyersEmail = json['contactedLawyersEmail'];
    this.contactedLawyersPhone = json['contactedLawyersPhone'];
    this.appointmentsDone = json['appointmentsDone'];
    this.lawyersChecked = json['lawyersChecked'];
    this.img = json['img'];
    this.canceledLawyers = json['canceledLawyers'];
  }
}
