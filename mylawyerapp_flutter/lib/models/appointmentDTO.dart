final String tableAppointments = 'appointments';

class AppointmentFields {
  static final List<String> values = [
    id,
    dateHour,
    description,
    clientId,
    lawyerId
  ];
  static final String id = '_id';
  static final String dateHour = 'dateHour';
  static final String description = 'description';
  static final String clientId = 'clientId';
  static final String lawyerId = 'lawyerId';
}

class Appointment {
  late String id;
  late DateTime dateHour;
  late String description;
  late String clientId;
  late String lawyerId;

  Appointment({
    required this.id,
    required this.dateHour,
    required this.description,
    required this.clientId,
    required this.lawyerId,
  });

  Appointment.fromJson(Map<String, dynamic> json) {
    this.id = json['_id'];
    this.dateHour = json['dateHour'];
    this.description = json['description'];
    this.clientId = json['clientId'];
    this.lawyerId = json['lawyerId'];
  }

  static Appointment fromJson2(Map<String, Object?> json) => Appointment(
        id: json[AppointmentFields.id] as String,
        dateHour: DateTime.parse(json[AppointmentFields.dateHour] as String),
        description: json[AppointmentFields.description] as String,
        clientId: json[AppointmentFields.clientId] as String,
        lawyerId: json[AppointmentFields.lawyerId] as String,
      );

  Map<String, Object?> toJson() => {
        AppointmentFields.id: id,
        AppointmentFields.dateHour: dateHour.toIso8601String(),
        AppointmentFields.description: description,
        AppointmentFields.clientId: clientId,
        AppointmentFields.lawyerId: lawyerId,
      };

  Appointment copy({
    String? id,
    DateTime? dateHour,
    String? description,
    String? clientId,
    String? lawyerId,
  }) =>
      Appointment(
        id: id ?? this.id,
        dateHour: dateHour ?? this.dateHour,
        description: description ?? this.description,
        clientId: clientId ?? this.clientId,
        lawyerId: lawyerId ?? this.lawyerId,
      );

  @override
  bool operator ==(other) {
    return (other is Appointment) &&
        other.id == id &&
        other.dateHour == dateHour &&
        other.description == description &&
        other.clientId == clientId &&
        other.lawyerId == lawyerId;
  }

  @override
  int get hashCode =>
      id.hashCode ^
      dateHour.hashCode ^
      description.hashCode ^
      clientId.hashCode ^
      lawyerId.hashCode;
}
