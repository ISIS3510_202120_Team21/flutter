final String tableCases = 'cases';

class CaseFields {
  static final List<String> values = [
    id,
    startDate,
    description,
    type,
    status,
    clientId,
    lawyerId,
    endDate,
    lawyerName
  ];
  static final String id = '_id';
  static final String startDate = 'startDate';
  static final String description = 'description';
  static final String type = 'type';
  static final String status = 'status';
  static final String clientId = 'clientId';
  static final String lawyerId = 'lawyerId';
  static final String endDate = 'endDate';
  static final String lawyerName = 'lawyerName';
}

class Case {
  late String id;
  late DateTime startDate;
  late String description;
  late String type;
  late String status;
  late String clientId;
  late String lawyerId;
  late String endDate;
  late String lawyerName;

  Case({
    required this.id,
    required this.startDate,
    required this.description,
    required this.type,
    required this.status,
    required this.clientId,
    required this.lawyerId,
    required this.endDate,
    required this.lawyerName,
  });

  Case.fromJson(Map<String, dynamic> json) {
    this.id = json['_id'];
    this.startDate = json['startDate'];
    this.description = json['description'];
    this.type = json['type'];
    this.status = json['status'];
    this.clientId = json['clientId'];
    this.lawyerId = json['lawyerId'];
    this.endDate = json['endDate'];
    this.lawyerName = json['lawyerName'];
  }

  static Case fromJson2(Map<String, Object?> json) => Case(
        id: json[CaseFields.id] as String,
        startDate: DateTime.parse(json[CaseFields.startDate] as String),
        description: json[CaseFields.description] as String,
        type: json[CaseFields.type] as String,
        status: json[CaseFields.status] as String,
        clientId: json[CaseFields.clientId] as String,
        lawyerId: json[CaseFields.lawyerId] as String,
        endDate: json[CaseFields.endDate] as String,
        lawyerName: json[CaseFields.lawyerName] as String,
      );

  Map<String, Object?> toJson() => {
        CaseFields.id: id,
        CaseFields.startDate: startDate.toIso8601String(),
        CaseFields.description: description,
        CaseFields.type: type,
        CaseFields.status: status,
        CaseFields.clientId: clientId,
        CaseFields.lawyerId: lawyerId,
        CaseFields.endDate: endDate,
        CaseFields.lawyerName: lawyerName,
      };

  Case copy({
    String? id,
    DateTime? startDate,
    String? description,
    String? type,
    String? status,
    String? clientId,
    String? lawyerId,
    String? endDate,
    String? lawyerName,
  }) =>
      Case(
        id: id ?? this.id,
        startDate: startDate ?? this.startDate,
        description: description ?? this.description,
        type: type ?? this.type,
        status: status ?? this.status,
        clientId: clientId ?? this.clientId,
        lawyerId: lawyerId ?? this.lawyerId,
        endDate: endDate ?? this.endDate,
        lawyerName: lawyerName ?? this.lawyerName,
      );

  @override
  bool operator ==(other) {
    return (other is Case) &&
        other.id == id &&
        other.startDate == startDate &&
        other.description == description &&
        other.type == type &&
        other.status == status &&
        other.clientId == clientId &&
        other.lawyerId == lawyerId &&
        other.endDate == endDate &&
        other.lawyerName == lawyerName;
  }

  @override
  int get hashCode =>
      id.hashCode ^
      startDate.hashCode ^
      description.hashCode ^
      type.hashCode ^
      status.hashCode ^
      clientId.hashCode ^
      lawyerId.hashCode ^
      endDate.hashCode ^
      lawyerName.hashCode;
}
