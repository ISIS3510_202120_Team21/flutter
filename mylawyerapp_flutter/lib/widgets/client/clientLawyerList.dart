import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/models/lawyerDTO.dart';
import 'package:mylawyerapp_flutter/widgets/client/client_lawyer_item.dart';
import 'package:mylawyerapp_flutter/providers/client_lawyerList_provider.dart';
import 'package:provider/provider.dart';
import 'package:location/location.dart';
import 'dart:math' show cos, sqrt, asin;

const _kFilters = <String, String>{
  'location': 'Location',
  'popularity': 'Popularity'
};

class ClientLawyerList extends StatefulWidget {
  @override
  _ClientLawyerListState createState() => _ClientLawyerListState();
}

class _ClientLawyerListState extends State<ClientLawyerList> {
  String _filter = 'Location';

  final imageURL = 'assets/images/userIcon.svg';

  late LocationData _currentPosition;

  Location location = Location();

  late List<Lawyer> _listSorted = [];
  List<Lawyer> listLawyersSorted = [];

  var isInit = true;

  @override
  Widget build(BuildContext context) {
    final lawyersData = Provider.of<clientLawyersProvider>(context);
    final loadedLawyers = lawyersData.lawyers;
    if (_filter == 'Location') {
      getLoc(loadedLawyers);
    } else if (_filter == 'Popularity') {
      getViews(_listSorted);
    }
    return _listSorted.length != 0
        ? Column(
            children: [
              _filterSelector(),
              Expanded(
                child: RefreshIndicator(
                  child: ListView.builder(
                    padding: const EdgeInsets.all(10.0),
                    itemCount: _listSorted.length,
                    itemBuilder: (ctx, i) => LawyerItem(
                        id: _listSorted[i].id,
                        idAuth: _listSorted[i].idAuth,
                        name: _listSorted[i].name,
                        phoneNumber: _listSorted[i].phoneNumber,
                        emailAddress: _listSorted[i].emailAddress,
                        yearOfExperience: _listSorted[i].yearOfExperience,
                        casesWon: _listSorted[i].casesWon,
                        rating: _listSorted[i].rating,
                        hourlyFee: _listSorted[i].hourlyFee,
                        field: _listSorted[i].field,
                        views: _listSorted[i].views,
                        distance: _listSorted[i].distance,
                        imageURL: imageURL),
                  ),
                  onRefresh: _getData,
                  color: Theme.of(context).primaryColor,
                ),
              )
            ],
          )
        : Center(
            child: CircularProgressIndicator(
            color: Theme.of(context).primaryColor,
          ));
    // : Center(
    //     child: Text('Try to fix your internet connection'),
    //   );
  }

  Future<void> _getData() async {
    isInit = true;
    if (_filter == 'Location') {
      getLoc(_listSorted);
    } else if (_filter == 'Popularity') {
      getViews(_listSorted);
    }
  }

  Widget _filterSelector() {
    final dropdown = DropdownButton(
      value: _filter,
      onChanged: (newFilter) {
        if (newFilter != null)
          setState(() {
            _filter = newFilter.toString();
          });
      },
      items: [
        for (final filter in _kFilters.values)
          DropdownMenuItem(
              value: filter,
              child: Text(
                filter.toString(),
              ))
      ],
    );
    return ListTile(
      contentPadding:
          const EdgeInsets.only(bottom: 1, left: 20, right: 20, top: 1),
      title: const Text('Filter'),
      trailing: dropdown,
    );
  }

  getViews(List<Lawyer> ll) {
    ll.sort((a, b) {
      if (a.views >= b.views) {
        return 1;
      }
      return -1;
    });

    setState(() {
      _listSorted = ll;
      isInit = true;
    });
  }

  void getLoc(List<Lawyer> ll) async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    if (isInit) {
      _currentPosition = await location.getLocation();
      //print(_currentPosition);

      int i = 0;
      List<String> distancias = [];
      ll.forEach((e) {
        var d = calculateDistance(
            double.parse(e.latitude),
            double.parse(e.longitude),
            _currentPosition.latitude,
            _currentPosition.longitude);

        distancias.add(i.toString() + '-' + d.toString());

        i++;
      });

      distancias.sort((a, b) {
        if (double.parse(a.split('-')[1]) >= double.parse(b.split('-')[1])) {
          return 1;
        }
        return -1;
      });
      //print(distancias);

      distancias.forEach((d) {
        print(d);
        int i = int.parse(d.split('-')[0]);
        int dist = double.parse(d.split('-')[1].toString()).ceil();
        ll[i].distance = dist;
        if (dist <= 50) {
          print(ll[i]);
          listLawyersSorted.add(ll[i]);
        }
        print("hola");
        print(listLawyersSorted);
      });

      setState(() {
        print(listLawyersSorted);
        _listSorted = listLawyersSorted;
      });
      isInit = false;
    }
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }
}
