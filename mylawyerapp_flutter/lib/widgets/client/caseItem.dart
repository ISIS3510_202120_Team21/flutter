import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/constants.dart';
import 'package:mylawyerapp_flutter/screens/caseDetail.dart';

class CaseItem extends StatelessWidget {
  late String number;
  late String id;
  late DateTime startDate;
  late String description;
  late String type;
  late String status;
  late String clientId;
  late String lawyerId;
  late String endDate;
  late String lawyerName;

  CaseItem({
    required this.number,
    required this.id,
    required this.startDate,
    required this.description,
    required this.type,
    required this.status,
    required this.clientId,
    required this.lawyerId,
    required this.endDate,
    required this.lawyerName,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.only(bottom: 10.0),
        elevation: 15,
        color: Theme.of(context).accentColor,
        child: Column(
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.fromLTRB(6, 10, 10, 0),
              title: Text(
                'Case #' + number,
                style: TextStyle(
                  color: cWhite,
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(1.0, 1.0),
                      blurRadius: 1.0,
                      color: cBlack,
                    ),
                    Shadow(
                      offset: Offset(1.0, 1.0),
                      blurRadius: 2.0,
                      color: cBlack,
                    ),
                  ],
                ),
              ),
              subtitle: Text(
                type +
                    ' issues \n' +
                    'Start date: ' +
                    startDate.toString().substring(0, 10) +
                    "\n" +
                    'Lawyer: ' +
                    lawyerName +
                    "\n" +
                    "Status: " +
                    status,
                style: TextStyle(
                  color: cBlack,
                ),
              ),
              leading: IconButton(
                icon: Icon(Icons.insert_drive_file_rounded),
                color: Theme.of(context).primaryColor,
                iconSize: 60,
                onPressed: () {},
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                IconButton(
                  onPressed: () async => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CaseDetail(
                                number: number,
                                type: type,
                                status: status,
                                description: description,
                                startDate: startDate,
                                lawyerName: lawyerName))),
                  },
                  icon: Icon(Icons.remove_red_eye_outlined),
                  color: Theme.of(context).primaryColor,
                ),
                SizedBox(width: 20),
              ],
            )
          ],
        ),
      ),
    );
  }
}
