import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/constants.dart';
import 'package:http/http.dart' as http;
import 'package:mylawyerapp_flutter/providers/auth.dart' as auth;

import 'package:mylawyerapp_flutter/screens/lawyerProfileDeatil.dart';

class LawyerItem extends StatelessWidget {
  late String id;
  late String idAuth;
  late String name;
  late String phoneNumber;
  late String emailAddress;
  late String yearOfExperience;
  late int casesWon;
  late int rating;
  late double hourlyFee;
  late String field;
  late int numberOfRatings;
  late String latitude;
  late String longitude;
  late int views;
  late int distance;

  final String imageURL;

  final String idClient = auth.getUserId().toString();
  LinkedHashMap<String, dynamic>? lawyersChecked = auth.getClient();

  LawyerItem({
    required this.id,
    required this.idAuth,
    required this.name,
    required this.phoneNumber,
    required this.emailAddress,
    required this.yearOfExperience,
    required this.casesWon,
    required this.rating,
    required this.hourlyFee,
    required this.field,
    required this.views,
    required this.distance,
    required this.imageURL,
  });

  _postInfoClients(BuildContext context, String urlClients,
      LinkedHashMap<String, dynamic>? lawyer) async {
    int numberLawyersChecked = lawyer!['lawyersChecked'];
    final response2 = await http.patch(
      Uri.parse(urlClients),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, int>{
        'lawyersChecked': numberLawyersChecked + 1,
      }),
    );
    final responseData2 = json.decode(response2.body);

    if (responseData2['error'] == null) {
      lawyersChecked!['lawyersChecked'] = numberLawyersChecked + 1;
      auth.setClient(lawyersChecked!);
    }
  }

  _postInfoLawyers(BuildContext context, String urlLaywers) async {
    print(views);
    final response1 = await http.patch(
      Uri.parse(urlLaywers),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, int>{'views': views + 1}),
    );
    final responseData = json.decode(response1.body);
    print(responseData);
    if (responseData['error'] == null) {
      views = views + 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    final backURL =
        ('https://mylawyer-flutter.herokuapp.com/api/lawyers/$idAuth');
    final backURLClients =
        ('https://mylawyer-flutter.herokuapp.com/api/clients/$idClient');

    return Container(
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.only(bottom: 10.0),
        elevation: 15,
        color: Theme.of(context).accentColor,
        child: Column(
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.fromLTRB(6, 10, 10, 0),
              title: Text(
                name,
                style: TextStyle(
                  color: cWhite,
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(1.0, 1.0),
                      blurRadius: 1.0,
                      color: cBlack,
                    ),
                    Shadow(
                      offset: Offset(1.0, 1.0),
                      blurRadius: 2.0,
                      color: cBlack,
                    ),
                  ],
                ),
              ),
              subtitle: Text(
                'With ' +
                    yearOfExperience +
                    ' years o experience in the field of ' +
                    field +
                    ', I am the right lawyer to resolve this issues. Do not hesitate to contact me on ' +
                    emailAddress +
                    '. \n \n $distance Km away',
                style: TextStyle(
                  color: cBlack,
                ),
              ),
              leading: IconButton(
                icon: Icon(Icons.person),
                color: Theme.of(context).primaryColor,
                iconSize: 60,
                onPressed: () {},
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                IconButton(
                  onPressed: () async => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LawyerProfileDetail(
                                  id: id,
                                  idAuth: idAuth,
                                  name: name,
                                  phoneNumber: phoneNumber,
                                  emailAddress: emailAddress,
                                  yearOfExperience: yearOfExperience,
                                  casesWon: casesWon,
                                  rating: rating,
                                  hourlyFee: hourlyFee,
                                  field: field,
                                  views: views,
                                  imageURL: "",
                                ))),
                    _postInfoClients(context, backURLClients, lawyersChecked),
                    _postInfoLawyers(context, backURL)
                  },
                  icon: Icon(Icons.remove_red_eye_outlined),
                  color: Theme.of(context).primaryColor,
                ),
                // SizedBox(width: 1),
                // IconButton(
                //   onPressed: () => {
                //     //This is where magic happens
                //   },
                //   icon: Icon(Icons.remove_circle_outline),
                //   color: Theme.of(context).primaryColor,
                // ),
                SizedBox(width: 20),
              ],
            )
          ],
        ),
      ),
    );
  }
}
