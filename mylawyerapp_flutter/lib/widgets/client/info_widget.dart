import 'package:flutter/material.dart';

class InfoWidget extends StatelessWidget {
  late String yearOfExperience;
  late int casesWon;
  late int rating;

  InfoWidget({
    required this.yearOfExperience,
    required this.casesWon,
    required this.rating,
  });

  @override
  Widget build(BuildContext context) => IntrinsicHeight(
        child: Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                buildButton(context, yearOfExperience, 'Experience (years)'),
                buildDivider(),
                buildButton(context, casesWon.toString(), 'Won cases'),
                buildDivider(),
                buildButton(context, rating.toString(), 'Rating'),
              ],
            ),
          ),
        ),
      );

  Widget buildDivider() => Container(height: 24, child: VerticalDivider());

  Widget buildButton(BuildContext context, String value, String text) =>
      (MaterialButton(
        padding: EdgeInsets.symmetric(vertical: 4),
        onPressed: () {},
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              value,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            SizedBox(height: 2),
            Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ));
}
