import 'package:flutter/material.dart';

AppBar buildAppBarCase(BuildContext context) {
  return AppBar(
    leading: BackButton(),
    elevation: 0,
    actions: [],
  );
}
