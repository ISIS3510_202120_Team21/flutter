import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/widgets/client/profile_pic.dart';
import 'package:mylawyerapp_flutter/widgets/client/profile_menu.dart';
import 'package:mylawyerapp_flutter/widgets/client/settings.dart';
import 'package:provider/provider.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          ProfilePic(),
          SizedBox(height: 20),
          ProfileMenu(
            text: "Settings",
            icon: "assets/images/Settings.svg",
            press: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SettingsPage(),
                  ));
            },
          ),
          ProfileMenu(
            text: "Help Center",
            icon: "assets/images/Question mark.svg",
            press: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text('Help Center'),
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in tristique ipsum. Donec ut commodo sem, at volutpat lectus. Suspendisse pellentesque placerat enim, nec mollis.')
                        ],
                      ),
                      actions: [
                        FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text("Close")),
                      ],
                    );
                  });
            },
          ),
          ProfileMenu(
            text: "Log Out",
            icon: "assets/images/Log out.svg",
            press: () {
              Navigator.of(context).pushReplacementNamed('/');
              Provider.of<Auth>(context, listen: false).logout();
            },
          ),
        ],
      ),
    );
  }
}
