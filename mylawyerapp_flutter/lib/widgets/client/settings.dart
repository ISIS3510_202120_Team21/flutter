import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mylawyerapp_flutter/db/shared_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:theme_provider/theme_provider.dart';
import '../../constants.dart';
import 'package:http/http.dart' as http;
import 'package:mylawyerapp_flutter/providers/auth.dart' as auth;
import 'package:mylawyerapp_flutter/screens/auth_screen.dart' as authScr;

class SettingsPage extends StatefulWidget {
  //const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  late String _name = '';
  final GlobalKey<FormState> _formKey = GlobalKey();
  String _password = '';
  int _phoneNumber = 0;
  String _oldPassword = '';
  late bool darkmMode = false;
  late bool notificationsNew = false;
  late bool accountActivity = false;
  late bool init = true;
  LinkedHashMap<String, dynamic>? clientInfo = auth.getClient();
  late String? clientId = auth.getUserId();

  @override
  Widget build(BuildContext context) {
    if (init) {
      init = false;
      setDarkMode();
    }
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        elevation: 0,
      ),
      body: Container(
        height: deviceSize.height,
        width: deviceSize.width,
        padding: EdgeInsets.only(left: 16, top: 25, right: 16),
        child: ListView(
          children: [
            SizedBox(
              height: 30,
            ),
            Row(
              children: [
                Icon(
                  Icons.settings_accessibility,
                  color: Theme.of(context).iconTheme.color,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "Accesibility",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),
            SizedBox(
              height: 2,
            ),
            buildNotificationOptionRow("Dark Mode", darkmMode),
            SizedBox(
              height: 2,
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              children: [
                Icon(Icons.person, color: Theme.of(context).iconTheme.color),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Account',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),
            SizedBox(
              height: 2,
            ),
            buildAccountOptionRow(context, "Change password"),
            SizedBox(
              height: 2,
            ),
            buildAccountOptionRow(context, "Change Name"),
            SizedBox(
              height: 2,
            ),
            buildAccountOptionRow(context, "Change Phone Number"),
            SizedBox(
              height: 2,
            ),
            buildAccountOptionRow(context, "Language"),
            SizedBox(
              height: 2,
            ),
            buildAccountOptionRow(context, "Privacy and security"),
            SizedBox(
              height: 2,
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              children: [
                Icon(
                  Icons.volume_up_outlined,
                  color: Theme.of(context).iconTheme.color,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "Notifications",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),
            SizedBox(
              height: 10,
            ),
            buildNotificationOptionRow("New for you", notificationsNew),
            buildNotificationOptionRow("Account activity", accountActivity),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  void setDarkMode() async {
    String url = 'https://mylawyer-flutter.herokuapp.com/api/clients/$clientId';
    final prefs = await SharedPreferences.getInstance();
    bool ck = prefs.containsKey('DarkMode');
    if (ck) {
      bool? dm = prefs.getBool('DarkMode');
      print(dm);
      setState(() {
        darkmMode = dm!;
      });
    }
  }

  Row buildNotificationOptionRow(String title, bool isActive) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: Colors.grey[600]),
        ),
        Transform.scale(
            scale: 0.7,
            child: CupertinoSwitch(
              value: isActive,
              onChanged: (bool val) async {
                if (title == 'New for you') {
                  setState(() {
                    notificationsNew = val;
                  });
                } else if (title == 'Account activity') {
                  setState(() {
                    accountActivity = val;
                  });
                } else {
                  init = false;
                  final prefs = await SharedPreferences.getInstance();
                  prefs.setBool('DarkMode', val);
                  setState(() {
                    if (val) {
                      ThemeProvider.controllerOf(context)
                          .setTheme('custom_dark');
                    } else {
                      ThemeProvider.controllerOf(context)
                          .setTheme('custom_light');
                    }

                    //ThemeProvider.controllerOf(context).notifyListeners();
                    darkmMode = val;
                  });
                }
              },
            ))
      ],
    );
  }

  GestureDetector buildAccountOptionRow(BuildContext context, String title) {
    final _passwordController = TextEditingController();
    String url = 'https://mylawyer-flutter.herokuapp.com/api/clients/$clientId';
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              if (title == 'Change password') {
                return Form(
                  key: _formKey,
                  child: AlertDialog(
                    title: Text(title),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: 'Old Password'),
                          obscureText: true,
                          onSaved: (value) {
                            _oldPassword = value!;
                          },
                        ),
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: 'New Password'),
                          obscureText: true,
                          controller: _passwordController,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 5) {
                              return 'Password is too short!';
                            }
                          },
                          onSaved: (value) {
                            _password = value!;
                          },
                        ),
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: 'Confirm Password'),
                          obscureText: true,
                          validator: (value) {
                            if (value != _passwordController.text) {
                              return 'Passwords do not match!';
                            }
                          },
                        )
                      ],
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            _formKey.currentState!.save();
                            //_changePassWord(context,_oldPassword,_password,url);
                          },
                          child: Text("Submitt")),
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text("Close")),
                    ],
                  ),
                );
              } else if (title == 'Change Name') {
                return Form(
                  key: _formKey,
                  child: AlertDialog(
                    title: Text(title),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Name'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Invalid name!';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _name = value!;
                            //globalUsername = value;
                          },
                        )
                      ],
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () {
                            _formKey.currentState!.save();
                            _changeName(context, _name, url);
                          },
                          child: Text("Submitt")),
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text("Close")),
                    ],
                  ),
                );
              } else if (title == 'Change Phone Number') {
                return Form(
                  key: _formKey,
                  child: AlertDialog(
                    title: Text(title),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: 'Phone number'),
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Invalid phone number!';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _phoneNumber = int.parse(value!);
                          },
                        )
                      ],
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () {
                            _formKey.currentState!.save();
                            _changePhoneNUmber(context, _phoneNumber, url);
                          },
                          child: Text("Submitt")),
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text("Close")),
                    ],
                  ),
                );
              } else if (title == 'Language') {
                return Form(
                  key: _formKey,
                  child: AlertDialog(
                    title: Text(title),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [Text('English (default)')],
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text("Close")),
                    ],
                  ),
                );
              } else {
                return Form(
                  key: _formKey,
                  child: AlertDialog(
                    title: Text(title),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in tristique ipsum. Donec ut commodo sem, at volutpat lectus. Suspendisse pellentesque placerat enim, nec mollis.')
                      ],
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text("Close")),
                    ],
                  ),
                );
              }
            });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Colors.grey[600],
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }

  _changePassWord(
      BuildContext context, String oldPasword, String newPassword, String url) {
    //TODO
  }

  _changeName(BuildContext context, String name, String url) async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await http.patch(
          Uri.parse(url),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'name': name,
          }),
        );

        final responseData = json.decode(response.body);
        print(responseData);
        if (responseData['error'] == null) {
          clientInfo!['name'] = name;
          auth.setClient(clientInfo!);
          UserPreferences().setGlobalClient(json.encode(clientInfo));
          authScr.setGlobalUserName(name);

          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text('Name changed succesfully'),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        Navigator.of(context)..pop()..pop();
                      },
                      child: Text('OK')),
                ],
              );
            },
          );
        }
      }
    } on SocketException catch (_) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(
                'You must have an internet connection to perform this action.'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.of(context)..pop();
                  },
                  child: Text('OK')),
            ],
          );
        },
      );
    }
  }

  _changePhoneNUmber(BuildContext context, int newPhoneNumb, String url) async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await http.patch(
          Uri.parse(url),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, int>{
            'phoneNumber': newPhoneNumb,
          }),
        );

        final responseData = json.decode(response.body);
        print(responseData);
        if (responseData['error'] == null) {
          clientInfo!['phoneNumber'] = newPhoneNumb;
          auth.setClient(clientInfo!);
          UserPreferences().setGlobalClient(json.encode(clientInfo));

          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text('Phone number changed succesfully'),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        Navigator.of(context)..pop()..pop();
                      },
                      child: Text('OK')),
                ],
              );
            },
          );
        }
      }
    } on SocketException catch (_) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(
                'You must have an internet connection to perform this action.'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.of(context)..pop();
                  },
                  child: Text('OK')),
            ],
          );
        },
      );
    }
  }
}
