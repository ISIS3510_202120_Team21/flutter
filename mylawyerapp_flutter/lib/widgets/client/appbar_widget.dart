import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'package:mylawyerapp_flutter/db/lawyers_database.dart';
import 'package:mylawyerapp_flutter/models/appointmentDTO.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart' as auth;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

const _kHours = <String>{'-', '14:00', '15:00'};

String _hour = '-';
String _chosedHour = '-';
bool pressed = false;
DateTime selectedDate = DateTime.now();
int appoiments = 0;
AppBar buildAppBar(BuildContext context, String lawyerId, String layerName,
    String field, String clientId, LinkedHashMap<String, dynamic>? clientInfo) {
  //final icon = CupertinoIcons.calendar_badge_plus;
  const icon = CupertinoIcons.calendar_badge_plus;
  String url = "https://mylawyer-flutter.herokuapp.com/api/appointments/";
  String url2 = 'https://mylawyer-flutter.herokuapp.com/api/clients/$clientId';

  appoiments = clientInfo!['appointmentsDone'];
  return AppBar(
    leading: BackButton(),
    elevation: 0,
    actions: [
      IconButton(
        icon: Icon(icon),
        onPressed: () async {
          try {
            final result = await InternetAddress.lookup('example.com');
            if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
              bool gotApptsLawyer = false;
              const urlAppts =
                  'https://mylawyer-flutter.herokuapp.com/api/appointments';
              final responseAppts = await http.get(Uri.parse(urlAppts));
              Map<String, dynamic> map = json.decode(responseAppts.body);
              List<dynamic> dataAppts = map['data']['data'];
              for (var i = 0; i < dataAppts.length; i++) {
                dynamic appt = dataAppts[i];
                if (appt['clientId'] == clientId &&
                    appt['lawyerId'] == lawyerId) {
                  gotApptsLawyer = true;
                  break;
                }
              }
              if (gotApptsLawyer) {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      content: Text(
                          'You already have an arranged appointment with lawyer ' +
                              layerName +
                              "."),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () {
                              Navigator.of(context)..pop();
                            },
                            child: Text('OK')),
                      ],
                    );
                  },
                );
              } else {
                //Implement analitics
                _displayTextInputDialog(context, lawyerId, layerName, field,
                    clientId, url, url2, clientInfo);
              }
            }
          } on SocketException catch (_) {
            showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  content: Text(
                      'You must have an internet connection to perform this action.'),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () {
                          Navigator.of(context)..pop();
                        },
                        child: Text('OK')),
                  ],
                );
              },
            );
          }
        },
      )
    ],
  );
}

_selectDate(BuildContext context, StateSetter setState) async {
  final DateTime? selected = await showDatePicker(
    context: context,
    initialDate: selectedDate,
    firstDate: DateTime.now(),
    lastDate: DateTime(2025),
  );
  if (selected != null && selected != selectedDate)
    setState(() {
      selectedDate = selected;
    });
}
/*
_getAppoiments(String url) async {
    getClient();
   final response = await http.get(Uri.parse(url));
    Map<String, dynamic> map = json.decode(response.body);
    List<dynamic> data = map['data']['data'];
    print(data);
    appoiments = data[0]['appointmentsDone'];
}*/

_putCounterOfAppoiments(BuildContext context, String clientId, String url,
    LinkedHashMap<String, dynamic>? clientInfo) async {
  final response = await http.patch(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, int>{
      'appointmentsDone': appoiments + 1,
    }),
  );

  final responseData = json.decode(response.body);
  if (responseData['error'] == null) {
    clientInfo!['appointmentsDone'] = appoiments + 1;
    appoiments = clientInfo['appointmentsDone'];
    auth.setClient(clientInfo);
  }
}

_postAppoimments(BuildContext context, DateTime date, String lawyerId,
    String layerName, String field, String clientId, String url) async {
  String description = 'Appoiment with lawyer $layerName. $field.';
  Map<String, String> customHeaders = {"content-type": "application/json"};
  //Post mongoDB
  final response = await http.post(
    Uri.parse(url),
    headers: customHeaders,
    body: json.encode(
      {
        'dateHour': date.toIso8601String(),
        'description': description,
        'clientId': clientId,
        'lawyerId': lawyerId
      },
    ),
  );
  //Post SQLite
  Map<String, dynamic> map = json.decode(response.body);
  final data = map['data']['data'];
  String idAppt = data["_id"];
  print(idAppt);
  Appointment newAppt = Appointment(
    id: idAppt,
    dateHour: date,
    description: description,
    clientId: clientId,
    lawyerId: lawyerId,
  );
  await LawyersDatabase.instance.createAppointment(newAppt);
  final responseData = json.decode(response.body);
  if (responseData['error'] != null) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text('Something happened'),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  _hour = '-';
                  pressed = false;
                  selectedDate = DateTime.now();
                  Navigator.of(context)..pop()..pop();
                },
                child: Text('OK')),
          ],
        );
      },
    );
    throw HttpException(responseData['error']['message']);
  } else {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text('Appointment done succesfully'),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  _hour = '-';
                  pressed = false;
                  selectedDate = DateTime.now();
                  Navigator.of(context)..pop()..pop();
                },
                child: Text('OK')),
          ],
        );
      },
    );
  }
}

_displayTextInputDialog(
    BuildContext context,
    String lawyerId,
    String layerName,
    String field,
    String clientId,
    String url,
    String url2,
    LinkedHashMap<String, dynamic>? clientInfo) async {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(
            'Select the appointment date and hour',
            textAlign: TextAlign.center,
          ),
          content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              var height = MediaQuery.of(context).size.height;
              var width = MediaQuery.of(context).size.width;
              return !pressed
                  ? Container(
                      height: height - 600,
                      width: width - 400,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                  "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}"),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    onPrimary: Colors.white,
                                    primary: Theme.of(context).buttonColor),
                                onPressed: () {
                                  _selectDate(context, setState);
                                },
                                child: Text("Choose Date"),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              DropdownButton(
                                value: _hour,
                                onChanged: (newHour) {
                                  if (newHour != null)
                                    setState(() {
                                      _hour = newHour.toString();
                                    });
                                },
                                items: [
                                  for (final hour in _kHours)
                                    DropdownMenuItem(
                                        value: hour,
                                        child: Text(
                                          hour.toString(),
                                        ))
                                ],
                              ),
                              Text('Choose Hour')
                            ],
                          ),
                        ],
                      ),
                    )
                  : Text('You already book an appointment');
            },
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.red,
              textColor: Colors.white,
              child: Text('CANCEL'),
              onPressed: () {
                _hour = '-';
                pressed = false;
                selectedDate = DateTime.now();
                Navigator.pop(context);
              },
            ),
            FlatButton(
              color: Colors.green,
              textColor: Colors.white,
              child: Text('OK'),
              onPressed: () async {
                if (_hour != '-') {
                  _chosedHour = _hour;

                  pressed = true;
                  //Send to the back the appoimment
                  //Do the analytics part
                  int hour = int.parse(_chosedHour.split(':')[0]);
                  int minutes = int.parse(_chosedHour.split(':')[1]);

                  String date = DateTime(selectedDate.year, selectedDate.month,
                          selectedDate.day, hour, minutes)
                      .toString();
                  _postAppoimments(context, DateTime.parse(date), lawyerId,
                      layerName, field, clientId, url);
                  _putCounterOfAppoiments(context, clientId, url2, clientInfo);
                  //_popNavigator(context);
                } else {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          content: Text('Please select an hour'),
                        );
                      });
                }
              },
            ),
          ],
        );
      });
}
