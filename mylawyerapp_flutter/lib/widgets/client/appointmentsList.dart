import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:mylawyerapp_flutter/db/lawyers_database.dart';
import 'package:mylawyerapp_flutter/models/appointmentDTO.dart';
import 'package:mylawyerapp_flutter/providers/appointments.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart';
import 'package:mylawyerapp_flutter/widgets/client/slidable_actions_enum.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';
import 'dart:io';
import 'package:mylawyerapp_flutter/providers/auth.dart' as auth;


class AppointmentsList extends StatefulWidget {
  @override
  _AppointmentsListState createState() => _AppointmentsListState();
}

class _AppointmentsListState extends State<AppointmentsList> {
  //late List<Appointment> _sortedList;
  var isInit = true;
  var isDeleted = false;
  var appointmentsData;
  late List<Appointment> loadedAppointments;
  late int canceledLawyers = auth.getClient()!['canceledLawyers'];
  late bool ini = true;
    
  @override
  Widget build(BuildContext context) {
    if (isInit) {
      Provider.of<Appointments>(context, listen: false).fetchAppointments();
    }


    if (isDeleted == false) {
      appointmentsData = Provider.of<Appointments>(context);
      loadedAppointments = appointmentsData.appointments;
    }

    loadedAppointments.sort((a, b) => a.dateHour.compareTo(b.dateHour));
    isInit = false;

    return loadedAppointments.length != 0
        ? ListView.builder(
            itemCount: loadedAppointments.length,
            itemBuilder: (context, index) {
              final item = loadedAppointments[index];

              return Slidable(
                key: UniqueKey(),

                //Descomentar para volver a la funcionalidad natural del widget (dismissable)

                // dismissal: SlidableDismissal(
                //   child: SlidableDrawerDismissal(),
                //   onDismissed: (type) {
                //     final action = type == SlideActionType.primary
                //         ? SlidableAction.voicecall
                //         : SlidableAction.delete;
                //     onDismissed(index, action);
                //   },
                // ),

                child: buildListTile(item),
                actionPane: SlidableStrechActionPane(),
                actions: <Widget>[
                  IconSlideAction(
                    caption: 'Voice call',
                    color: Colors.green,
                    icon: Icons.phone,
                    onTap: () => onDismissed(
                      index,
                      SlidableAction.voicecall,
                    ),
                  ),
                  IconSlideAction(
                    caption: 'Email',
                    color: Colors.indigo,
                    icon: Icons.mail,
                    onTap: () => onDismissed(index, SlidableAction.email),
                  )
                ],
                secondaryActions: <Widget>[
                  IconSlideAction(
                    caption: 'Cancel',
                    color: Colors.red,
                    icon: Icons.cancel,
                    onTap: () => onDismissed(index, SlidableAction.delete),
                  ),
                ],
              );
            },
          )
        // : Center(
        //     child: CircularProgressIndicator(
        //     color: Theme.of(context).primaryColor,
        //   ));
        : Center(
            child: Text('You do not have any appointments at the moment.'),
          );
  }

  void onDismissed(int index, SlidableAction action) {
    final item = loadedAppointments[index];

    switch (action) {
      case SlidableAction.voicecall:
        callMailLawyer(index, 1);
        break;
      case SlidableAction.email:
        callMailLawyer(index, 2);
        break;
      case SlidableAction.delete:
        deleteAppointment(index, item.id);
        break;
    }
  }

  Future<void> callMailLawyer(index, option) async {
    String? lawyerId = loadedAppointments[index].lawyerId;
    String getURL =
        "https://mylawyer-flutter.herokuapp.com/api/lawyers/$lawyerId";

    String? clientId = getUserId();
    String clientBackURL =
        "https://mylawyer-flutter.herokuapp.com/api/clients/$clientId";

    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await http.get(Uri.parse(getURL));
        Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> data = map['data']['data'];
        if (option == 1) {
          final lPhoneNumber = data[0]['phoneNumber'];
          launch("tel://$lPhoneNumber");
          int contactedLawyersPhone = getClient()!["contactedLawyersPhone"];
          contactedLawyersPhone++;
          await http.patch(
            Uri.parse(clientBackURL),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, int>{
              'contactedLawyersPhone': contactedLawyersPhone,
            }),
          );
          var actualClient = getClient();
          actualClient!['contactedLawyersPhone'] = contactedLawyersPhone;
          setClient(actualClient);
        } else if (option == 2) {
          final lEmail = data[0]['emailAddress'];
          print(lEmail);
          launch("mailto:$lEmail");
          int contactedLawyersEmail = getClient()!["contactedLawyersEmail"];
          contactedLawyersEmail++;
          await http.patch(
            Uri.parse(clientBackURL),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, int>{
              'contactedLawyersEmail': contactedLawyersEmail,
            }),
          );
          var actualClient = getClient();
          actualClient!['contactedLawyersEmail'] = contactedLawyersEmail;
          setClient(actualClient);
        }
      }
    } on SocketException catch (_) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(
                'You must have an internet connection to perform this action.'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.of(context)..pop();
                  },
                  child: Text('OK')),
            ],
          );
        },
      );
    } catch (e) {
      throw e;
    }
  }

  Future<void> deleteAppointment(index, appointmentId) async {
    String? clientId = getUserId();
    String url =
        "https://mylawyer-flutter.herokuapp.com/api/clients/$clientId";
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        //Implent BQ
        final response1 = await http.patch(
          Uri.parse(url),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, int>{'canceledLawyers': canceledLawyers + 1}),
        );
        final responseData = json.decode(response1.body);
        print(responseData);
        if (responseData['error'] == null) {
          canceledLawyers = canceledLawyers + 1;
        }

        bool wantsToDelete = false;
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Text('Are you sure you want to cancel the appointment?'),
              actions: <Widget>[
                TextButton(
                    onPressed: () async {
                      Navigator.of(context)..pop();
                      await LawyersDatabase.instance
                          .deleteAppointment(appointmentId);
                      String deleteURL =
                          "https://mylawyer-flutter.herokuapp.com/api/appointments/$appointmentId";
                      try {
                        http.delete(Uri.parse(deleteURL), headers: {
                          'Content-Type': 'application/json',
                          'Accept': 'application/json',
                        });
                      } catch (e) {
                        throw e;
                      }

                      setState(() {
                        loadedAppointments.removeAt(index);
                        isInit = true;
                        isDeleted = true;
                      });
                    },
                    child: Text('Yes')),
                TextButton(
                    onPressed: () {
                      Navigator.of(context)..pop();
                    },
                    child: Text('No')),
              ],
            );
          },
        );
      }
    } on SocketException catch (_) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(
                'You must have an internet connection to perform this action.'),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.of(context)..pop();
                  },
                  child: Text('OK')),
            ],
          );
        },
      );
    }
  }

  final image = Image.network(
    "https://www.tsensor.online/wp-content/uploads/2020/04/avatar-icon-png-105-images-in-collection-page-3-avatarpng-512_512.png",
    errorBuilder:
        (BuildContext context, Object exception, StackTrace? stackTrace) {
      return Container(
        child: Text(
          "No internet",
          style: TextStyle(fontSize: 10),
        ),
        padding: EdgeInsets.all(10),
      );
    },
    fit: BoxFit.cover,
    width: 128,
    height: 128,
  );

  Widget buildListTile(Appointment item) => Builder(
        builder: (context) => ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 16,
          ),
          leading: CircleAvatar(radius: 28, child: image),
          title: Text(
            item.description,
            style: TextStyle(fontSize: 18),
          ),
          subtitle: Text(
            item.dateHour.toString().substring(0, 16),
            style: TextStyle(fontSize: 16),
          ),
          onTap: () {
            final slidable = Slidable.of(context)!;
            final isClosed =
                slidable.renderingMode == SlidableRenderingMode.none;

            if (isClosed) {
              slidable.open();
            } else {
              slidable.close();
            }
          },
        ),
      );
}
