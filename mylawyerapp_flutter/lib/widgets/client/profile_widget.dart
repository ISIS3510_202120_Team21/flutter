import 'dart:io';
import 'package:flutter/material.dart';

class ProfileWidget extends StatelessWidget {
  final String imagePath;
  final double size;

  const ProfileWidget({
    Key? key,
    required this.imagePath,
    required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final color = Theme.of(context).colorScheme.primary;

    return Center(
      child: Stack(
        children: [
          buildImage(),
        ],
      ),
    );
  }

  Widget buildImage() {
    final image = Image.network(
      imagePath,
      errorBuilder:
          (BuildContext context, Object exception, StackTrace? stackTrace) {
        return Container(
          child: Text("Img not available. No connection."),
          padding: EdgeInsets.all(10),
        );
      },
      fit: BoxFit.cover,
      width: size,
      height: size,
    );

    return ClipOval(
      child: Material(
        color: Colors.transparent,
        child: image,
        // child: Ink.image(
        //   image: image,
        //   fit: BoxFit.cover,
        //   width: 128,
        //   height: 128,
        // ),
      ),
    );
  }
}
