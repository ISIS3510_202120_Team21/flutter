import 'dart:collection';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mylawyerapp_flutter/screens/auth_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../constants.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart' as auth;

class ProfilePic extends StatefulWidget {
  @override
  _ProfilePicState createState() => _ProfilePicState();
}

class _ProfilePicState extends State<ProfilePic> {
  late File _imageFile = new File("path");
  final _picker = ImagePicker();
  bool _load = false;
  bool isInit = false;
  bool isInit2 = true;

  int cont = 0;

  String? userName = "";

  String globalPath = '/data/user/0/com.example.mylawyerapp_flutter/cache/';

  final String _idClient = auth.getUserId().toString();

  LinkedHashMap<String, dynamic>? globalClient = auth.getClient();
  //bool val = false;
  @override
  Widget build(BuildContext context) {
    //getDatabasesPath().then((value) => print(value.toString()));
    userName = getGlobalUserName()!.substring(0, 2);
    print('sssdasd');
    File prov;
    _getNumberFromMemory().then((value) => {
          if (isInit2)
            {
              cont = value!,
              prov = File(globalPath + 'profilePic$cont-$_idClient.jpg'),
              prov.exists().then((value3) => {
                    if (value3)
                      {
                        setState(() {
                          print(prov);
                          _load = true;
                          this._imageFile = prov;
                        })
                      }
                  }),
              isInit2 = false,
            }
        });
    print("CONT " + cont.toString());

    _isCachedImage().then((value) => {
          //_searchForImageInDirectory(),
          if (!isInit)
            {
              print("ENTRA ISINIT " + isInit.toString()),
              if (!_load)
                {
                  print("ENTRA LOAD " + _load.toString()),
                  setState(() {
                    isInit = true;
                    if (value) {
                      _getPathImage().then((value2) => {
                            print(value2),
                            if (value2!.contains(".jpg"))
                              {
                                setState(() {
                                  _load = value;
                                  this._imageFile = File(value2);
                                })
                              }
                          });
                    } else {}
                  })
                }
            }
        });

    return SizedBox(
      height: 115,
      width: 115,
      child: Stack(
        fit: StackFit.expand,
        clipBehavior: Clip.none,
        children: [
          // ignore: unnecessary_null_comparison

          if (!this._load)
            CircleAvatar(
              backgroundColor: cGray,
              child: Text(userName!,
                  style: TextStyle(fontSize: 55, color: Colors.white)),
            )
          else
            CircleAvatar(
              backgroundImage: FileImage(this._imageFile),
              backgroundColor: cGray,
            ),

          Positioned(
            right: -16,
            bottom: 0,
            child: SizedBox(
              height: 46,
              width: 46,
              child: TextButton(
                style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                    side: BorderSide(color: Colors.white),
                  ),
                  primary: Colors.white,
                  backgroundColor: Color(0xFFF5F6F9),
                ),
                onPressed: () async => _pickImageFromCamera(),
                child: SvgPicture.asset("assets/images/Camera Icon.svg"),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> _pickImageFromCamera() async {
    final PickedFile? pickedFile =
        // ignore: deprecated_member_use
        await _picker.getImage(source: ImageSource.camera);

    File temp = File(pickedFile!.path);
    setState(() {
      cont = cont + 1;
    });

    _saveNumberToMemory(cont);
    var newPath =
        changeFileNameOnlySync(temp, 'profilePic$cont-$_idClient.jpg');

    print(newPath.path);

    _deleteUntilCont(_idClient, cont);

    _saveImageToMemory(newPath.path);
    setState(() {
      this._imageFile = newPath;
      _load = true;
    });
  }

  File changeFileNameOnlySync(File file, String newFileName) {
    var path = file.path;
    var lastSeparator = path.lastIndexOf(Platform.pathSeparator);
    var newPath = path.substring(0, lastSeparator + 1) + newFileName;
    return file.renameSync(newPath);
  }

  Future<bool> _isCachedImage() async {
    final prefs = await SharedPreferences.getInstance();
    final isData = prefs.containsKey('UserImageProfile');
    return isData;
  }

  Future<String?> _getPathImage() async {
    final prefs = await SharedPreferences.getInstance();
    final isData = prefs.getString('UserImageProfile');
    return isData;
  }

  _saveImageToMemory(String path) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('UserImageProfile', path);
  }

  _saveNumberToMemory(int n) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt('Number', n);
  }

  Future<int?> _getNumberFromMemory() async {
    final prefs = await SharedPreferences.getInstance();
    var hay = await prefs.containsKey('Number');
    if (hay) {
      return await prefs.getInt('Number');
    } else {
      return 0;
    }
  }



  _deleteUntilCont(String id, int c) {
    var systemTempDir = Directory(globalPath);
    var lista = systemTempDir.list();
    lista.forEach((element) {
      var p = element.path;
      p = p.split('/')[6];
      var idP = p.split('-')[1].split('.')[0];
      print(p.split('-')[0]);
      print(p.split('-')[0].substring(9, p.split('-')[0].length));
      var n = int.parse(p.split('-')[0].substring(10, p.split('-')[0].length));
      if (idP == id && n < c) {
        element.delete();
      }
    });
  }
}
