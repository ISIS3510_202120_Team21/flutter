import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/models/caseDTO.dart';
import 'package:mylawyerapp_flutter/widgets/client/caseItem.dart';
import 'package:provider/provider.dart';
import 'package:mylawyerapp_flutter/providers/cases.dart';

class CasesList extends StatefulWidget {
  const CasesList({Key? key}) : super(key: key);

  @override
  _CasesListState createState() => _CasesListState();
}

class _CasesListState extends State<CasesList> {
  var isInit = true;
  var isDeleted = false;
  var casesData;
  late List<Case> loadedCases;

  @override
  Widget build(BuildContext context) {
    if (isInit) {
      Provider.of<Cases>(context, listen: false).fetchCases();
    }

    if (isDeleted == false) {
      casesData = Provider.of<Cases>(context);
      loadedCases = casesData.cases;
    }

    loadedCases.sort((a, b) => a.startDate.compareTo(b.startDate));
    isInit = false;

    return loadedCases.length != 0
        ? Column(
            children: [
              Expanded(
                  child: ListView.builder(
                      padding: const EdgeInsets.all(10.0),
                      itemCount: loadedCases.length,
                      itemBuilder: (ctx, i) => CaseItem(
                          number: (i + 1).toString(),
                          id: loadedCases[i].id,
                          startDate: loadedCases[i].startDate,
                          description: loadedCases[i].description,
                          type: loadedCases[i].type,
                          status: loadedCases[i].status,
                          clientId: loadedCases[i].clientId,
                          lawyerId: loadedCases[i].lawyerId,
                          endDate: loadedCases[i].endDate,
                          lawyerName: loadedCases[i].lawyerName)))
            ],
          )
        : Center(
            child: Text('You do not have any cases at the moment.'),
          );
  }
}
