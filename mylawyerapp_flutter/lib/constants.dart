import 'package:flutter/material.dart';

Color cBrown = Color(0xFFAF9164);
Color cBone = Color(0xFF2f7F3E3);
Color cGray = Color(0xFFB3B6B7);
Color cWhite = Color(0xFFFFFFFF);
Color cBlack = Color(0xFF000000);

Color cBackground = Color(0xFFFFFFFF);