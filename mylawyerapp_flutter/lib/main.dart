import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/providers/cases.dart';
import 'package:mylawyerapp_flutter/screens/splash_screen.dart';
import 'package:mylawyerapp_flutter/screens/auth_screen.dart';
import 'package:mylawyerapp_flutter/screens/splash_screen2.dart';
import 'package:mylawyerapp_flutter/theme.dart';
import 'package:provider/provider.dart';
import 'constants.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart';

import 'providers/client_lawyerList_provider.dart';
import 'providers/appointments.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/services.dart';
import 'package:theme_provider/theme_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runZonedGuarded(() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((value) => runApp(MyApp()));
  }, (error, stackTrace) {
    //FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => Auth()),
        ChangeNotifierProvider(create: (ctx) => Appointments([])),
        ChangeNotifierProvider(create: (ctx) => Cases([])),
        ChangeNotifierProxyProvider<Auth, clientLawyersProvider>(
          create: (context) => clientLawyersProvider([]),
          update: (
            context,
            auth,
            prevClientLawyersList,
          ) =>
              prevClientLawyersList!
                ..update(
                    auth,
                    prevClientLawyersList.lawyers == null
                        ? []
                        : prevClientLawyersList.lawyers),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => ThemeProvider(
          themes: [
            MyThemes.lightTheme,
            MyThemes.darkTheme,
          ],
          child: Builder(
            builder: (context) => MaterialApp(
                title: 'MyLawyer App',
                theme: ThemeProvider.themeOf(context).data,
                home: auth.isAuth
                    ? SplashScreen()
                    : FutureBuilder(
                        future: auth.tryAutoLogin(),
                        builder: (ctx, authResultSnapshot) =>
                            authResultSnapshot.connectionState ==
                                    ConnectionState.waiting
                                ? SplashScreen2()
                                : AuthScreen(),
                      ),
                routes: {}),
          ),
        ),
      ),
    );
  }
}
