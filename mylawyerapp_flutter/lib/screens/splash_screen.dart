import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mylawyerapp_flutter/constants.dart';
import '../screens/client_overview_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {

    
    super.initState();
    var d = Duration(seconds: 3);
    //delayed 3 secs to next page (home_screen)
    Future.delayed(d, () {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) {
            return ClientOverViewScreen();
          },
        ),
        (route) => false,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                cGray.withOpacity(0.5),
                cBrown.withOpacity(0.9),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0, 1],
            ),
          ),
        ),
        SingleChildScrollView(
          child: Container(
            height: deviceSize.height,
            width: deviceSize.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Container(
                    height: 120,
                    width: 120,
                    color: Colors.transparent,
                    child: SvgPicture.asset('assets/images/logoBalance.svg'),
                  ),
                ),
                Text(
                  "Loading...",
                  style: TextStyle(
                    color: cBlack,
                    fontSize: 20,
                    fontStyle: FontStyle.italic,
                  ),
                )
              ],
            ),
          ),
        )
      ],
    ));
  }
}
