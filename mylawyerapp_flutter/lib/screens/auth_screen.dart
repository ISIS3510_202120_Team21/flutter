import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart';
import 'package:provider/provider.dart';
import 'package:mylawyerapp_flutter/models/http_exception.dart';
import 'package:flutter/services.dart';

enum AuthMode { Signup, Login }

String? globalUsername = "";

String? getGlobalUserName() {
  return globalUsername;
}

String? setGlobalUserName(userName) {
  globalUsername = userName;
}

class AuthScreen extends StatelessWidget {
  static const routeName = '/auth';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                cGray.withOpacity(0.5),
                cBrown.withOpacity(0.9),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0, 1],
            ),
          ),
        ),
        SingleChildScrollView(
          child: Container(
            height: deviceSize.height,
            width: deviceSize.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    child: Container(
                  height: deviceSize.height * 0.45,
                  width: deviceSize.width * 0.45,
                  color: Colors.transparent,
                  child: SvgPicture.asset('assets/images/logoBalance.svg'),
                )),
                Flexible(
                  child: Container(
                    child: Text(
                      'MyLawyer',
                      style: TextStyle(
                          color: cBlack,
                          fontSize: 50,
                          fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                AuthCard(),
              ],
            ),
          ),
        )
      ],
    ));
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard({
    Key? key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  bool valueFirst = false;

  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.Login;
  Map<String, String> _authData = {
    'email': '',
    'password': '',
    'phoneNumber': '',
    'role': 'client',
    'name': ''
  };
  var _isLoading = false;
  final _passwordController = TextEditingController();

  void _showErrorDialog(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('An error occured!'),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(ctx).pop();
                  },
                )
              ],
            ));
  }

  Future<void> _submit() async {
    if (!_formKey.currentState!.validate()) {
      // Invalid!
      return;
    }
    _formKey.currentState!.save();
    setState(() {
      _isLoading = true;
    });

    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (_authMode == AuthMode.Login) {
          // Log user in
          await Provider.of<Auth>(context, listen: false).login(
            _authData['email'],
            _authData['password'],
          );
        } else {
          // Sign user up
          await Provider.of<Auth>(context, listen: false).signup(
            _authData['name'],
            _authData['phoneNumber'],
            _authData['role'],
            _authData['email'],
            _authData['password'],
          );
        }
      }
    } on HttpException catch (error) {
      var errorMessage = 'Authentication failed';
      if (error.toString().contains('EMAIL_EXISTS')) {
        errorMessage = 'This email address is already in use.';
      } else if (error.toString().contains('INVALID_EMAIL')) {
        errorMessage = 'This is not a valid email address.';
      } else if (error.toString().contains('WEAK_PASSWORD')) {
        errorMessage = 'This password is too weak.';
      } else if (error.toString().contains('EMAIL_NOT_FOUND')) {
        errorMessage = 'Could not find a user with that email.';
      } else if (error.toString().contains('INVALID_PASSWORD')) {
        errorMessage = 'Invalid password.';
      }
      _showErrorDialog(errorMessage);
    } on SocketException catch (_) {
      _showErrorDialog('You need internet conection to login');
    } catch (error) {
      const errorMessage =
          'Could not authenticate you. Please try again later.';
      _showErrorDialog(errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  void _switchAuthMode() {
    if (_authMode == AuthMode.Login) {
      setState(() {
        _authMode = AuthMode.Signup;
      });
    } else {
      setState(() {
        _authMode = AuthMode.Login;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: Container(
        height: _authMode == AuthMode.Signup ? 480 : 260,
        constraints:
            BoxConstraints(minHeight: _authMode == AuthMode.Signup ? 480 : 260),
        width: deviceSize.width * 0.75,
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                if (_authMode == AuthMode.Signup)
                  Column(children: <Widget>[
                    TextFormField(
                      enabled: _authMode == AuthMode.Signup,
                      decoration: InputDecoration(labelText: 'Name'),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Invalid name!';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _authData['name'] = value!;
                        globalUsername = value;
                      },
                    ),
                    TextFormField(
                      enabled: _authMode == AuthMode.Signup,
                      decoration: InputDecoration(labelText: 'Phone number'),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Invalid phone number!';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _authData['phoneNumber'] = value!;
                      },
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Laywer?',
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 17,
                          ),
                        ),
                        Checkbox(
                          checkColor: cBlack,
                          activeColor: cBrown,
                          value: this.valueFirst,
                          onChanged: null
                          // (bool? value) {
                          //   setState(() {
                          //     this.valueFirst = value!;
                          //     if (this.valueFirst == true) {
                          //       _authData['role'] = 'lawyer';
                          //     } else {
                          //       _authData['role'] = 'client';
                          //     }
                          //   });
                          // }
                          ,
                        ),
                      ],
                    ),
                  ]),
                TextFormField(
                  decoration: InputDecoration(labelText: 'E-Mail'),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value!.isEmpty || !value.contains('@')) {
                      return 'Invalid email!';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _authData['email'] = value!;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Password'),
                  obscureText: true,
                  controller: _passwordController,
                  validator: (value) {
                    if (value!.isEmpty || value.length < 5) {
                      return 'Password is too short!';
                    }
                  },
                  onSaved: (value) {
                    _authData['password'] = value!;
                  },
                ),
                if (_authMode == AuthMode.Signup)
                  TextFormField(
                    enabled: _authMode == AuthMode.Signup,
                    decoration: InputDecoration(labelText: 'Confirm Password'),
                    obscureText: true,
                    validator: _authMode == AuthMode.Signup
                        ? (value) {
                            if (value != _passwordController.text) {
                              return 'Passwords do not match!';
                            }
                          }
                        : null,
                  ),
                SizedBox(
                  height: 20,
                ),
                if (_isLoading)
                  CircularProgressIndicator(
                    color: cBrown,
                  )
                else
                  RaisedButton(
                    child:
                        Text(_authMode == AuthMode.Login ? 'LOGIN' : 'SIGN UP'),
                    onPressed: _submit,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    padding:
                        EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
                    color: cBrown,
                    textColor: Theme.of(context).primaryTextTheme.button!.color,
                  ),
                FlatButton(
                  child: Text(
                      '${_authMode == AuthMode.Login ? 'SIGNUP' : 'LOGIN'} INSTEAD'),
                  onPressed: _switchAuthMode,
                  padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  textColor: cBrown,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
