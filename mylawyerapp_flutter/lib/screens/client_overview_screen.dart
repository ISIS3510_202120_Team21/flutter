import 'package:mylawyerapp_flutter/providers/appointments.dart';
import 'package:mylawyerapp_flutter/providers/cases.dart';
import 'package:mylawyerapp_flutter/providers/client_lawyerList_provider.dart';
import 'package:mylawyerapp_flutter/widgets/client/appointmentsList.dart';
import 'package:mylawyerapp_flutter/widgets/client/casesList.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:theme_provider/theme_provider.dart';
import '../widgets/client/clientLawyerList.dart';
import '../widgets/client/clientProfile.dart';

class ClientOverViewScreen extends StatefulWidget {
  const ClientOverViewScreen({Key? key}) : super(key: key);

  @override
  _ClientOverViewScreenState createState() => _ClientOverViewScreenState();
}

class _ClientOverViewScreenState extends State<ClientOverViewScreen> {
  dynamic defaultWidget = ClientLawyerList();
  var widgetLoaded = false;
  var _isInit = true;
  final _kPages = <String, IconData>{
    'Lawyers': Icons.search,
    'Appts': Icons.calendar_today,
    'Cases': Icons.folder_rounded,
    'Profile': Icons.person,
    //'Settings': Icons.settings,
  };

  final List<Widget> _screens = [
    ClientLawyerList(),
    AppointmentsList(),
    CasesList(),
    ClientProfile(),
    //ClientSettings(),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      Provider.of<clientLawyersProvider>(context, listen: false).fetchLawyers();
      Provider.of<Appointments>(context, listen: false).fetchAppointments();
      Provider.of<Cases>(context, listen: false).fetchCases();
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    ThemeProvider.controllerOf(context).addListener(() {
      loadConfiguration(context);
    });

    if (!widgetLoaded) {
      loadConfiguration(context);
      widgetLoaded = true;
    }

    return defaultWidget;
  }

  void loadConfiguration(BuildContext context) async {
    setState(() {
      defaultWidget = DefaultTabController(
        initialIndex: 1,
        length: _kPages.length,
        child: Scaffold(
          appBar: AppBar(
            leading: Container(),
            title: Text("MyLawyer"),
          ),
          body: SafeArea(
            child: TabBarView(
              children: _screens,
            ),
          ),
          bottomNavigationBar: Container(
            width: double.infinity,
            child: ConvexAppBar(
              backgroundColor: ThemeProvider.themeOf(context)
                  .data
                  .bottomNavigationBarTheme
                  .backgroundColor,
              items: <TabItem>[
                for (final entry in _kPages.entries)
                  TabItem(icon: entry.value, title: entry.key),
              ],
              onTap: (int i) async => {
                i == 0
                    ? await Provider.of<clientLawyersProvider>(context,
                            listen: false)
                        .fetchLawyers()
                    : false,
                i == 1
                    ? Provider.of<Appointments>(context, listen: false)
                        .fetchAppointments()
                    : false,
                i == 2
                    ? Provider.of<Cases>(context, listen: false).fetchCases()
                    : false,
              },
            ),
          ),
        ),
      );
    });
  }
}
