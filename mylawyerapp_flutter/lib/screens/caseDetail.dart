import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/widgets/client/appbar_widget_case.dart';
import 'package:mylawyerapp_flutter/widgets/client/profile_widget.dart';

class CaseDetail extends StatefulWidget {
  late String number;
  late String type;
  late String status;
  late String description;
  late DateTime startDate;
  late String lawyerName;

  CaseDetail(
      {required this.number,
      required this.type,
      required this.status,
      required this.description,
      required this.startDate,
      required this.lawyerName});

  @override
  _CaseDetailState createState() => _CaseDetailState(
      number: number,
      type: type,
      status: status,
      description: description,
      startDate: startDate,
      lawyerName: lawyerName);
}

class _CaseDetailState extends State<CaseDetail> {
  late String number;
  late String type;
  late String status;
  late String description;
  late DateTime startDate;
  late String lawyerName;

  _CaseDetailState(
      {required this.number,
      required this.type,
      required this.status,
      required this.description,
      required this.startDate,
      required this.lawyerName});

  @override
  Widget build(BuildContext context) {
    bool connection = true;

    return Scaffold(
      appBar: buildAppBarCase(context),
      body: SingleChildScrollView(
        child: Column(children: [buildInfo(), buildComments()]),
      ),
    );
  }

  Widget buildInfo() => Column(
        children: [
          Text("Case #" + number,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              )),
          const SizedBox(
            height: 10,
          ),
          Text("Status: " + status,
              style: TextStyle(
                fontSize: 18,
              )),
          const SizedBox(
            height: 10,
          ),
          Text("Type: " + type + " issues.",
              style: TextStyle(
                fontSize: 18,
              )),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Text("Lawyer: " + lawyerName,
                  style: TextStyle(
                    fontSize: 18,
                  )),
              const SizedBox(
                width: 5,
              ),
              ProfileWidget(
                imagePath:
                    "https://www.ourlawr.com/wp-content/uploads/2018/02/corporate.jpg?6bfec1&6bfec1",
                size: 64,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          const SizedBox(
            height: 45,
          ),
          Text("Description:",
              style: TextStyle(
                fontSize: 18,
              )),
          const SizedBox(
            height: 5,
          ),
          Text(description,
              style: TextStyle(
                fontSize: 18,
              )),
          const SizedBox(
            height: 40,
          ),
          Text("Additional information:",
              style: TextStyle(
                fontSize: 18,
              )),
          const SizedBox(
            height: 5,
          ),
          Text(
              "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
              style: TextStyle(
                fontSize: 18,
              )),
          const SizedBox(
            height: 40,
          ),
        ],
      );

  Widget buildComments() => Column(children: [
        Text("Comments",
            style: TextStyle(
              fontSize: 18,
            )),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            const SizedBox(
              width: 5,
            ),
            Text(lawyerName + ":",
                style: TextStyle(
                  fontSize: 18,
                )),
            const SizedBox(
              width: 5,
            ),
            Text("Lorem ipsum dolor sit amet.",
                style: TextStyle(
                  fontSize: 18,
                )),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            const SizedBox(
              width: 5,
            ),
            Text(lawyerName + ":",
                style: TextStyle(
                  fontSize: 18,
                )),
            const SizedBox(
              width: 5,
            ),
            Text("Duis aute irure dolor.",
                style: TextStyle(
                  fontSize: 18,
                )),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ]);
}
