import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/db/lawyers_database.dart';
import 'package:mylawyerapp_flutter/models/caseDTO.dart';
import 'package:mylawyerapp_flutter/widgets/client/appbar_widget.dart';
import 'package:mylawyerapp_flutter/widgets/client/hireButton_widget.dart';
import 'package:mylawyerapp_flutter/widgets/client/info_widget.dart';
import 'package:mylawyerapp_flutter/widgets/client/profile_widget.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart' as auth;
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';

class LawyerProfileDetail extends StatefulWidget {
  //const LawyerProfileDetail({Key? key}) : super(key: key);

  late String id;
  late String idAuth;
  late String name;
  late String phoneNumber;
  late String emailAddress;
  late String yearOfExperience;
  late int casesWon;
  late int rating;
  late double hourlyFee;
  late String field;
  late int numberOfRatings;
  late String latitude;
  late String longitude;
  late int views;
  final String imageURL;

  LawyerProfileDetail({
    required this.id,
    required this.idAuth,
    required this.name,
    required this.phoneNumber,
    required this.emailAddress,
    required this.yearOfExperience,
    required this.casesWon,
    required this.rating,
    required this.hourlyFee,
    required this.field,
    required this.views,
    required this.imageURL,
  });

  @override
  _LawyerProfileDetailState createState() => _LawyerProfileDetailState(
        id: id,
        idAuth: idAuth,
        name: name,
        phoneNumber: phoneNumber,
        emailAddress: emailAddress,
        yearOfExperience: yearOfExperience,
        casesWon: casesWon,
        rating: rating,
        hourlyFee: hourlyFee,
        field: field,
        views: views,
        imageURL: "",
      );
}

class _LawyerProfileDetailState extends State<LawyerProfileDetail> {
  late String id;
  late String idAuth;
  late String name;
  late String phoneNumber;
  late String emailAddress;
  late String yearOfExperience;
  late int casesWon;
  late int rating;
  late double hourlyFee;
  late String field;
  late int numberOfRatings;
  late String latitude;
  late String longitude;
  late int views;
  final String imageURL;

  final String idClient = auth.getUserId().toString();
  final LinkedHashMap<String, dynamic>? clientInfo = auth.getClient();
  _LawyerProfileDetailState({
    required this.id,
    required this.idAuth,
    required this.name,
    required this.phoneNumber,
    required this.emailAddress,
    required this.yearOfExperience,
    required this.casesWon,
    required this.rating,
    required this.hourlyFee,
    required this.field,
    required this.views,
    required this.imageURL,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, idAuth, name, field, idClient, clientInfo),
      body: ListView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(4),
        children: [
          ProfileWidget(
            imagePath:
                "https://www.ourlawr.com/wp-content/uploads/2018/02/corporate.jpg?6bfec1&6bfec1",
            size: 128,
          ),
          const SizedBox(
            height: 5,
          ),
          buildName(name, emailAddress),
          const SizedBox(
            height: 5,
          ),
          Center(child: buildHireButton()),
          const SizedBox(
            height: 16,
          ),
          InfoWidget(
            yearOfExperience: yearOfExperience,
            casesWon: casesWon,
            rating: rating,
          ),
          const SizedBox(
            height: 34,
          ),
          buildAbout(),
        ],
      ),
    );
  }

  Widget buildName(String name, String emailAddress) => Column(
        children: [
          Text(name,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              )),
          const SizedBox(
            height: 5,
          ),
          Text(field + " lawyer",
              style: TextStyle(
                fontSize: 18,
              )),
          Text(
            emailAddress,
            style: TextStyle(color: Colors.grey),
          ),
          Text(
            phoneNumber,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildHireButton() => HireButton(
        text: 'Hire!',
        onClicked: () async {
          try {
            final result = await InternetAddress.lookup('example.com');
            if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
              const urlLawyer =
                  'https://mylawyer-flutter.herokuapp.com/api/lawyers?idAuth=bVqPYlQpdkNHztdl88ugDD6Eivs2';
              final responseLawyer = await http.get(Uri.parse(urlLawyer));
              Map<String, dynamic> mapLawyer = json.decode(responseLawyer.body);
              dynamic dataLaywer = mapLawyer['data']['data'];
              print(mapLawyer);
              print(dataLaywer[0]['name']);

              //Try to find a case with this specific lawyer. If the user already has one, he/she can not hire him/her again.
              String? userId = auth.getUserId();
              bool gotCasesLawyer = false;
              const urlCases =
                  'https://mylawyer-flutter.herokuapp.com/api/cases';
              final responseCases = await http.get(Uri.parse(urlCases));
              Map<String, dynamic> map = json.decode(responseCases.body);
              List<dynamic> dataCases = map['data']['data'];
              for (var i = 0; i < dataCases.length; i++) {
                dynamic pCase = dataCases[i];
                if (pCase['clientId'] == userId &&
                    pCase['lawyerId'] == idAuth &&
                    (pCase['status'] == 'Pending approval by lawyer' ||
                        pCase['status'] == 'Ongoing')) {
                  gotCasesLawyer = true;
                  break;
                }
              }

              if (gotCasesLawyer) {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      content: Text(
                          'You cannot hire because you already have an ongoing/pending case with the lawyer ' +
                              name +
                              "."),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () {
                              Navigator.of(context)..pop();
                            },
                            child: Text('OK')),
                      ],
                    );
                  },
                );
              } else {
                //Try to find an appointment with that specific lawyer. If it exists, check that the appointment's date is past

                const url =
                    'https://mylawyer-flutter.herokuapp.com/api/appointments';
                final response = await http.get(Uri.parse(url));
                Map<String, dynamic> map = json.decode(response.body);
                List<dynamic> data = map['data']['data'];
                bool gotApptsLawyer = false;
                dynamic choseAppt;
                for (var i = 0; i < data.length; i++) {
                  dynamic appt = data[i];
                  if (appt['clientId'] == userId &&
                      appt['lawyerId'] == idAuth) {
                    gotApptsLawyer = true;
                    choseAppt = appt;
                    break;
                  }
                }
                if (gotApptsLawyer) {
                  final f = new DateFormat('yyyy-MM-dd');

                  DateTime nowF = DateTime.parse(f.format(DateTime.now()));
                  DateTime apptF = DateTime.parse(
                      f.format(DateTime.parse(choseAppt['dateHour'])));

                  DateTime nowDate = DateTime.now();

                  if (apptF.compareTo(nowF) <= 0) {
                    //Creation
                    Map<String, String> customHeaders = {
                      "content-type": "application/json"
                    };
                    final response = await http.post(
                      Uri.parse(urlCases),
                      headers: customHeaders,
                      body: json.encode(
                        {
                          'startDate': nowDate.toIso8601String(),
                          'description': field +
                              " case with lawyer " +
                              name +
                              ". Client: " +
                              auth.globalClient!['name'],
                          'type': field,
                          'status': 'Pending approval by lawyer',
                          'clientId': auth.globalClient!['idAuth'],
                          'lawyerId': idAuth,
                          'endDate': "",
                          'lawyerName': name
                        },
                      ),
                    );

                    //Post SQLite
                    Map<String, dynamic> map = json.decode(response.body);
                    final data = map['data']['data'];
                    String idCase = data["_id"];
                    Case newCase = Case(
                        id: idCase,
                        startDate: nowDate,
                        description: field +
                            " case with lawyer " +
                            name +
                            ". Client: " +
                            auth.globalClient!['name'],
                        type: field,
                        status: 'Pending approval by lawyer',
                        clientId: auth.globalClient!['idAuth'],
                        lawyerId: idAuth,
                        endDate: "",
                        lawyerName: name);
                    await LawyersDatabase.instance.createCase(newCase);

                    //Alert
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          content: Text(field +
                              ' case successfully created. You can check it in the Cases section. Please wait until the lawyer approves it.'),
                          actions: <Widget>[
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context)..pop();
                                },
                                child: Text('OK')),
                          ],
                        );
                      },
                    );
                  } else {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          content: Text(
                              'You cannot hire because you have not yet discussed about your specific case with the lawyer in your already arranged appointment.'),
                          actions: <Widget>[
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context)..pop();
                                },
                                child: Text('OK')),
                          ],
                        );
                      },
                    );
                  }
                } else {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        content: Text(
                            'You cannot hire because you have not yet created an appointment with the lawyer to discuss about your specific case.'),
                        actions: <Widget>[
                          TextButton(
                              onPressed: () {
                                Navigator.of(context)..pop();
                              },
                              child: Text('OK')),
                        ],
                      );
                    },
                  );
                }
              }
            }
          } on SocketException catch (_) {
            showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  content: Text(
                      'You must have an internet connection to perform this action.'),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () {
                          Navigator.of(context)..pop();
                        },
                        child: Text('OK')),
                  ],
                );
              },
            );
          }
        },
      );

  Widget buildAbout() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'About',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 16),
            Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
              style: TextStyle(fontSize: 16, height: 1.4),
            )
          ],
        ),
      );
}
