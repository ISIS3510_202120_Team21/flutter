import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mylawyerapp_flutter/db/lawyers_database.dart';
import 'dart:convert';
import 'dart:io';
import 'package:mylawyerapp_flutter/models/caseDTO.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart';
import 'package:collection/collection.dart';

class Cases with ChangeNotifier {
  bool globalLostConnectionCases = false;
  List<Case> _cases = [];
  List<Case> casesSQL = [];

  Cases(this._cases);

  List<Case> get cases {
    return [..._cases];
  }

  void update(authToken, cases) {
    _cases = cases;
    notifyListeners();
  }

  Future<List<Case>> fetchCases() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        const url = 'https://mylawyer-flutter.herokuapp.com/api/cases';
        final response = await http.get(Uri.parse(url));
        String? userId = getUserId();
        Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> data = map['data']['data'];
        final List<Case> loadedCases = [];
        data.forEach((pCase) {
          if (userId == pCase['clientId']) {
            loadedCases.add(
              Case(
                id: pCase['_id'],
                startDate: DateTime.parse(pCase['startDate']),
                description: pCase['description'],
                type: pCase['type'],
                status: pCase['status'],
                clientId: pCase['clientId'],
                lawyerId: pCase['lawyerId'],
                endDate: pCase['endDate'],
                lawyerName: pCase['lawyerName'],
              ),
            );
          }
        });
        _cases = loadedCases;
        //If there's connectivity. It will store the cases data in SQLite (if the lists are different and it is the first time that the app initiates)
        Function unOrdDeepEq = const DeepCollectionEquality.unordered().equals;
        List<Case> checkTemp = await LawyersDatabase.instance.readAllCases();
        List<Case> checkTemp2 = [];
        checkTemp.forEach((element) {
          if (element.clientId == globalUserId) {
            checkTemp2.add(element);
          }
        });
        casesSQL = checkTemp2;
        if (_cases.length != casesSQL.length) {
          casesSQL = await LawyersDatabase.instance.readAllCases();
          print("different length appts?: " +
              (_cases.length != casesSQL.length).toString());
          if (globalLostConnectionCases || (_cases.length != casesSQL.length)) {
            print("Writing in SQLite cases...");
            //DB is created if not exists already
            await LawyersDatabase.instance.database;
            //Insert the difference between the lists in MongoDB and SQLite into SQLite
            if (!unOrdDeepEq(_cases, casesSQL)) {
              print("HAY CAMBIOS cases");
              //delete
              List<Case> temp2 = [...casesSQL];
              temp2.removeWhere((e) => _cases.contains(e));
              // temp2.forEach((element) {
              //   if (element.clientId == userId) {
              //     print(element.lawyerId);
              //   }
              // });
              temp2.forEach((deleteCase) async {
                if (deleteCase.clientId == userId) {
                  await LawyersDatabase.instance.deleteCase(deleteCase.id);
                }
              });
              //insert
              List<Case> temp = [..._cases];
              temp.removeWhere((e) => casesSQL.contains(e));
              temp.forEach((newCase) async {
                if (newCase.clientId == userId) {
                  await LawyersDatabase.instance.createCase(newCase);
                }
              });
            }
          }
        }
        globalLostConnectionCases = false;
      }
      notifyListeners();
      return _cases;
    } on SocketException catch (_) {
      globalLostConnectionCases = true;
      await LawyersDatabase.instance.database;
      List<Case> tempList = await LawyersDatabase.instance.readAllCases();
      List<Case> tempList2 = [];
      tempList.forEach((element) {
        if (element.clientId == globalUserId) {
          tempList2.add(element);
        }
      });
      _cases = tempList2;
      return _cases;
    } catch (error) {
      throw error;
    }
  }
}
