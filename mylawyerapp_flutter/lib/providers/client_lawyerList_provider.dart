import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:mylawyerapp_flutter/db/lawyers_database.dart';
import 'package:mylawyerapp_flutter/models/lawyerDTO.dart';
import 'package:collection/collection.dart';
import 'package:mylawyerapp_flutter/providers/auth.dart';
import 'dart:io';

class clientLawyersProvider with ChangeNotifier {
  bool globalLostConnectionLawyers = false;
  List<Lawyer> _lawyers;
  List<Lawyer> lawyersSQL = [];

  clientLawyersProvider(this._lawyers /* , this.authToken */);

  List<Lawyer> get lawyers {
    return [..._lawyers];
  }

  void update(authToken, lawyers) {
    _lawyers = lawyers;
    //_authToken = authToken;
    notifyListeners();
  }

  Future<void> fetchLawyers() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("CONNECTED!!!");
        const url = 'https://mylawyer-flutter.herokuapp.com/api/lawyers';
        final response = await http.get(Uri.parse(url));

        Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> data = map['data']['data'];
        print(data);
        final List<Lawyer> loadedLawyers = [];
        data.forEach((lawyer) {
          loadedLawyers.add(
            Lawyer(
              id: lawyer['_id'],
              idAuth: lawyer['idAuth'],
              name: lawyer['name'],
              phoneNumber: lawyer['phoneNumber'],
              emailAddress: lawyer['emailAddress'],
              yearOfExperience: lawyer['yearOfExperience'],
              casesWon: lawyer['casesWon'],
              rating: lawyer['rating'],
              hourlyFee: double.parse(lawyer['hourlyFee'].toString()),
              field: lawyer['field'],
              numberOfRatings: lawyer['numberOfRatings'],
              longitude: lawyer['longitude'],
              latitude: lawyer['latitude'],
              views: lawyer['views'],
              distance: 0,
            ),
          );
        });
        _lawyers = loadedLawyers;

        //If there's connectivity. It will store the lawyers data in SQLite (if the lists are different and it is the first time that the app initiates)
        Function unOrdDeepEq = const DeepCollectionEquality.unordered().equals;
        print("appInit: " + globalAppInit.toString());
        print("lostConnection: " + globalLostConnectionLawyers.toString());
        //Read all lawyers in SQLite
        if (globalAppInit || (_lawyers.length != lawyersSQL.length)) {
          lawyersSQL = await LawyersDatabase.instance.readAllLawyers();
        }
        print("different length?: " +
            (_lawyers.length != lawyersSQL.length).toString());
        if (globalAppInit ||
            globalLostConnectionLawyers ||
            (_lawyers.length != lawyersSQL.length)) {
          print("Writing in SQLite...");
          globalAppInit = false;
          //DB is created if not exists already
          await LawyersDatabase.instance.database;
          //Insert the difference between the lists in MongoDB and SQLite into SQLite
          if (!unOrdDeepEq(_lawyers, lawyersSQL)) {
            print("HAY CAMBIOS");
            //delete
            List<Lawyer> temp2 = [...lawyersSQL];
            temp2.removeWhere((e) => _lawyers.contains(e));
            temp2.forEach((element) {
              print(element.name);
            });
            temp2.forEach((deleteLawyer) async {
              await LawyersDatabase.instance.deleteLawyer(deleteLawyer.idAuth);
            });
            //insert
            List<Lawyer> temp = [..._lawyers];
            temp.removeWhere((e) => lawyersSQL.contains(e));
            temp.forEach((newLawyer) async {
              await LawyersDatabase.instance.createLawyer(newLawyer);
            });
          }
        }
        globalLostConnectionLawyers = false;
        //await LawyersDatabase.instance.close();
      }
      notifyListeners();
    } on SocketException catch (_) {
      print("NO CONNECTION!!!");
      globalLostConnectionLawyers = true;
      globalAppInit = false;
      await LawyersDatabase.instance.database;
      _lawyers = await LawyersDatabase.instance.readAllLawyers();
      //await LawyersDatabase.instance.close();
    } catch (error) {
      throw (error);
    }
  }
}
