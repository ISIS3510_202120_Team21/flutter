import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:mylawyerapp_flutter/db/shared_preferences.dart';
import 'package:mylawyerapp_flutter/models/http_exception.dart';
import 'package:mylawyerapp_flutter/screens/auth_screen.dart';
import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

late String? globalUserId;
late LinkedHashMap<String, dynamic>? globalClient;
bool globalAppInit = false;

class Auth with ChangeNotifier {
  String? _token;
  DateTime? _expiryDate;
  String? _userId;
  String? _role;
  Timer? _authTimer;

  bool get isAuth {
    return token != null;
  }

  String? get rol {
    return _role;
  }

  String? get token {
    if (_expiryDate != null &&
        _expiryDate!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    } else {
      UserPreferences().deletePreferences();
    }
    return null;
  }

  Future<void> setParameters() async {
    _token = await UserPreferences().getFirebaseToken();
    _userId = await UserPreferences().getUserId();
    globalUserId = _userId;
    final globalClientLoaded = await UserPreferences().getGlobalClient();
    final list = jsonDecode(globalClientLoaded!);
    globalClient = new LinkedHashMap<String, dynamic>.from(list);
    var date = await UserPreferences().getExpiryDate();
    date = date.toString();
    _expiryDate = DateTime.parse(date);
  }

  String APIKey = 'AIzaSyBHkcJRGxBeRUxrnZeR3v_daNv1EzfahLE';

  Future<void> _authenticate(String? name, String? phoneNumber, String? role,
      String email, String password, String urlSegment) async {
    globalAppInit = true;
    final url =
        'https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=$APIKey';

    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await http.post(
          Uri.parse(url),
          body: json.encode(
            {
              'email': email,
              'password': password,
              'returnSecureToken': true,
            },
          ),
        );
        final responseData = json.decode(response.body);
        if (responseData['error'] != null) {
          throw HttpException(responseData['error']['message']);
        }
        _token = responseData['idToken'];
        _userId = responseData['localId'];
        globalUserId = _userId;
        _expiryDate = DateTime.now().add(
          Duration(
            seconds: int.parse(
              responseData['expiresIn'],
            ),
          ),
        );

        final firebaseURLUsers = Uri.https(
            'mylawyer-5e72d-default-rtdb.firebaseio.com', '/$_userId.json');

        //_role = role;
        var endpoint = role == 'client' ? 'clients' : 'lawyers';
        final backURL =
            ('https://mylawyer-flutter.herokuapp.com/api/$endpoint');
        //signup
        if (urlSegment == 'signUp') {
          //Firebase
          http.put(firebaseURLUsers,
              body: json.encode(
                {'role': role},
              ));
          //Mongo
          http.post(
            Uri.parse(backURL),
            headers: {"Content-Type": "application/json"},
            body: json.encode({
              "idAuth": _userId,
              "name": name,
              "phoneNumber": phoneNumber,
              "emailAddress": email,
              "hasMeetings": false,
              "contactedLawyersEmail": 0,
              "contactedLawyersPhone": 0,
              "appointmentsDone": 0,
              "lawyersChecked": 0,
              "img": "",
              "contImg": 0,
              "canceledLawyers": 0
            }),
          );
          String url =
              'https://mylawyer-flutter.herokuapp.com/api/clients/$_userId';
          final response = await http.get(Uri.parse(url));
          Map<String, dynamic> map = json.decode(response.body);
          List<dynamic> data = map['data']['data'];
          globalClient = data[0];

          //User Preferences
          //setFirebaseToken(_userId);
          fetchUserInfo();
        }
        fetchUserInfo();       
        _autoLogout();
        notifyListeners();
        //Shared preferences
        final userData = json.encode(
          {
            'token': _token,
            'userId': _userId,
            'expiryDate': _expiryDate!.toIso8601String()
          },
        );
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('UserData', userData);
      }
    } on SocketException catch (_) {
      print('not connected');
    } catch (error) {
      throw error;
    }
  }

  Future<void> signup(String? name, String? phoneNumber, String? role,
      String? email, String? password) async {
    return _authenticate(
        name!, phoneNumber!, role!, email!, password!, 'signUp');
  }

  Future<void> login(String? email, String? password) async {
    return _authenticate(
        null, null, null, email!, password!, 'signInWithPassword');
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    final isData = prefs.containsKey('UserData');
    if (isData == true) {
      final data = prefs.getString('UserData');

      final extractedData = json.decode(data!);

      final expiryDate = DateTime.parse(extractedData['expiryDate'].toString());
      if (expiryDate.isBefore(DateTime.now())) {
        return false;
      }
      _token = extractedData['token'].toString();
      _userId = extractedData['userId'].toString();
      _expiryDate = expiryDate;
      globalUserId = _userId;
      var globalClientLoaded = await UserPreferences().getGlobalClient();
      final globalClientL2 = globalClientLoaded.toString();
      final list = json.decode(globalClientL2);
      globalClient = new LinkedHashMap<String, dynamic>.from(list);
      setGlobalUserName(globalClient!['name']);

      _autoLogout();
      //print(fetchUserInfo());
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  void logout() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer!.cancel();
      _authTimer = null;
    }

    SharedPreferences preferences = await SharedPreferences.getInstance();

    await preferences.remove('FirebaseToken');
    await preferences.remove('GlobalClient');
    await preferences.remove('UserId');
    await preferences.remove('ExpiryDate');
    await preferences.remove('UserData');
    //await preferences.remove('Number');
    final isData = await preferences.containsKey('UserImageProfile');
    if (isData) {
      await preferences.remove('UserImageProfile');
    }
    int? imgCont = preferences.getInt("Number");
    String url = "https://mylawyer-flutter.herokuapp.com/api/clients/$globalUserId";
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        await http.patch(
          Uri.parse(url),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, int>{
            'contImg': imgCont!,
          }),
        );
      }
    } on SocketException catch (_) {} catch (e) {}

    await preferences.remove('Number');
    await preferences.remove('DarkMode');
    notifyListeners();
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer!.cancel();
    }
    final timeToExpiry = _expiryDate!.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}

Future<LinkedHashMap<String, dynamic>?> fetchUserInfo() async {
  try {
    final result = await InternetAddress.lookup('example.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      String? userId = globalUserId;
      String url = 'https://mylawyer-flutter.herokuapp.com/api/clients/$userId';
      final response = await http.get(Uri.parse(url));
      Map<String, dynamic> map = json.decode(response.body);
      List<dynamic> data = map['data']['data'];
      globalClient = data[0];
      UserPreferences().setGlobalClient(json.encode(globalClient));
      setGlobalUserName(data[0]['name']);
      final prefs = await SharedPreferences.getInstance();
      prefs.setInt("Number", globalClient!['contImg']);
      print(globalClient!["contImg"]);
      return data[0];
    }
  } on SocketException catch (_) {
    print('not connecterd 2');
  } catch (e) {
    throw e;
  }
}

LinkedHashMap<String, dynamic>? getClient() {
  return globalClient;
}

void setClient(LinkedHashMap<String, dynamic> client) {
  globalClient = client;
}

String? getUserId() {
  return globalUserId;
}
