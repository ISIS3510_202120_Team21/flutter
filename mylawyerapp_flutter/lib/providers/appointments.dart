import 'package:flutter/material.dart';
import 'package:mylawyerapp_flutter/db/lawyers_database.dart';
import 'package:mylawyerapp_flutter/models/appointmentDTO.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:mylawyerapp_flutter/providers/auth.dart';
import 'package:collection/collection.dart';

class Appointments with ChangeNotifier {
  bool globalLostConnectionAppointments = false;
  List<Appointment> _appointments = [];
  List<Appointment> appointmentsSQL = [];

  Appointments(this._appointments);

  List<Appointment> get appointments {
    return [..._appointments];
  }

  void update(authToken, appointments) {
    _appointments = appointments;
    notifyListeners();
  }

  Future<List<Appointment>> fetchAppointments() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("CONNECTED appts!!!");
        const url = 'https://mylawyer-flutter.herokuapp.com/api/appointments';
        final response = await http.get(Uri.parse(url));

        String? userId = getUserId();

        //print(json.decode(response.body));
        Map<String, dynamic> map = json.decode(response.body);
        List<dynamic> data = map['data']['data'];
        final List<Appointment> loadedAppointments = [];
        // data.forEach((appointment) {
        //   if (userId == appointment['clientId']) {
        //     loadedAppointments.add(
        //       Appointment(
        //         id: appointment['_id'],
        //         dateHour: DateTime.parse(appointment['dateHour']),
        //         description: appointment['description'],
        //         clientId: appointment['clientId'],
        //         lawyerId: appointment['lawyerId'],
        //       ),
        //     );
        //   }
        // });

        for (var i = 0; i < data.length; i++) {
          dynamic appointment = data[i];
          if (userId == appointment['clientId']) {
            loadedAppointments.add(
              Appointment(
                id: appointment['_id'],
                dateHour: DateTime.parse(appointment['dateHour']),
                description: appointment['description'],
                clientId: appointment['clientId'],
                lawyerId: appointment['lawyerId'],
              ),
            );
          }
        }
        _appointments = loadedAppointments;

        //If there's connectivity. It will store the appointments data in SQLite (if the lists are different and it is the first time that the app initiates)
        Function unOrdDeepEq = const DeepCollectionEquality.unordered().equals;
        print("lostConnection appts: " +
            globalLostConnectionAppointments.toString());
        // print("appts: " + _appointments.length.toString());
        // print("apptsSql: " + appointmentsSQL.length.toString());
        List<Appointment> checkTemp =
            await LawyersDatabase.instance.readAllAppointments();
        List<Appointment> checkTemp2 = [];

        // checkTemp.forEach((element) {
        //   if (element.clientId == globalUserId) {
        //     checkTemp2.add(element);
        //   }
        // });

        for (var i = 0; i < checkTemp.length; i++) {
          Appointment element = checkTemp[i];
          if (element.clientId == globalUserId) {
            checkTemp2.add(element);
          }
        }

        appointmentsSQL = checkTemp2;
        if (_appointments.length != appointmentsSQL.length) {
          appointmentsSQL =
              await LawyersDatabase.instance.readAllAppointments();
          print("different length appts?: " +
              (_appointments.length != appointmentsSQL.length).toString());
          if (globalLostConnectionAppointments ||
              (_appointments.length != appointmentsSQL.length)) {
            print("Writing in SQLite appts...");
            //DB is created if not exists already
            await LawyersDatabase.instance.database;
            //Insert the difference between the lists in MongoDB and SQLite into SQLite
            if (!unOrdDeepEq(_appointments, appointmentsSQL)) {
              print("HAY CAMBIOS appts");
              //delete
              List<Appointment> temp2 = [...appointmentsSQL];
              temp2.removeWhere((e) => _appointments.contains(e));
              // temp2.forEach((element) {
              //   if (element.clientId == userId) {
              //     print(element.lawyerId);
              //   }
              // });
              // temp2.forEach((deleteAppointment) async {
              //   if (deleteAppointment.clientId == userId) {
              //     await LawyersDatabase.instance
              //         .deleteAppointment(deleteAppointment.id);
              //   }
              // });

              for (var i = 0; i < temp2.length; i++) {
                Appointment deleteAppointment = temp2[i];
                if (deleteAppointment.clientId == userId) {
                  await LawyersDatabase.instance
                      .deleteAppointment(deleteAppointment.id);
                }
              }

              //insert
              List<Appointment> temp = [..._appointments];
              temp.removeWhere((e) => appointmentsSQL.contains(e));
              // temp.forEach((newAppointment) async {
              //   if (newAppointment.clientId == userId) {
              //     await LawyersDatabase.instance
              //         .createAppointment(newAppointment);
              //   }
              // });

              for (var i = 0; i < temp.length; i++) {
                Appointment newAppointment = temp[i];
                if (newAppointment.clientId == userId) {
                  await LawyersDatabase.instance
                      .createAppointment(newAppointment);
                }
              }
            }
          }
        }
        globalLostConnectionAppointments = false;
      }
      notifyListeners();
      return _appointments;
    } on SocketException catch (_) {
      print("NO CONNECTION appts!!!");
      globalLostConnectionAppointments = true;
      await LawyersDatabase.instance.database;
      List<Appointment> tempList =
          await LawyersDatabase.instance.readAllAppointments();
      List<Appointment> tempList2 = [];
      // tempList.forEach((element) {
      //   if (element.clientId == globalUserId) {
      //     tempList2.add(element);
      //   }
      // });

      for (var i = 0; i < tempList.length; i++) {
        Appointment element = tempList[i];
        if (element.clientId == globalUserId) {
          tempList2.add(element);
        }
      }
      _appointments = tempList2;
      return _appointments;
      //await LawyersDatabase.instance.close();
    } catch (error) {
      throw (error);
    }
  }
}
